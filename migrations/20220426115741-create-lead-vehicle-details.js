'use strict';
module.exports = {
  async up(queryInterface, Sequelize) {
    await queryInterface.createTable('LeadVehicleDetails', {
      id: {
        allowNull: false,
        autoIncrement: true,
        primaryKey: true,
        type: Sequelize.INTEGER
      },
      lead_id: {
        type: Sequelize.INTEGER
      },
      vehicle_category: {
        type: Sequelize.STRING
      },
      vehicle_type: {
        type: Sequelize.STRING
      },
      make: {
        type: Sequelize.STRING
      },
      registration_year: {
        type: Sequelize.INTEGER
      },
      model: {
        type: Sequelize.STRING
      },
      material_code: {
        type: Sequelize.STRING
      },
      material_group: {
        type: Sequelize.STRING
      },
      kerb_weight: {
        type: Sequelize.STRING
      },
      scrap_price: {
        type: Sequelize.NUMERIC
      },
      variant: {
        type: Sequelize.STRING
      },
      fuel_type: {
        type: Sequelize.STRING
      },
      cng_lpg_addition: {
        type: Sequelize.STRING
      },
      vehicle_condition: {
        type: Sequelize.STRING
      },
      month_year_of_manufacture: {
        type: Sequelize.DATE
      },
      registration_number: {
        type: Sequelize.TEXT
      },
      date_of_registration: {
        type: Sequelize.DATE
      },
      engine_number: {
        type: Sequelize.STRING
      },
      chassis_number: {
        type: Sequelize.STRING
      },
      rc_expiry_date: {
        type: Sequelize.DATE
      },
      is_Seller_and_rc_owner_different: {
        type: Sequelize.STRING
      },
      base_price: {
        type: Sequelize.NUMERIC
      },
      threshold_value: {
        type: Sequelize.NUMERIC
      },
      upper_max_price: {
        type: Sequelize.NUMERIC
      },
      demanded_price: {
        type: Sequelize.NUMERIC
      },
      final_price: {
        type: Sequelize.NUMERIC
      },
      comment: {
        type: Sequelize.TEXT
      },
      approval_required: {
        type: Sequelize.BOOLEAN
      },
      approval_required_from: {
        type: Sequelize.INTEGER
      },
      approval_from_user_id: {
        type: Sequelize.INTEGER
      },
      approval_request_date: {
        type: Sequelize.DATE
      },
      approval_status: {
        type: Sequelize.STRING
      },
      approval_date: {
        type: Sequelize.DATE
      },
      approval_comment: {
        type: Sequelize.STRING
      },
      deal_status: {
        type: Sequelize.STRING
      },
      deal_convertted_date: {
        type: Sequelize.DATE
      },
      converted_by: {
        type: Sequelize.INTEGER
      },
      updated_date: {
        type: Sequelize.DATE
      },
      updated_by: {
        type: Sequelize.INTEGER
      },
      created_by: {
        type: Sequelize.INTEGER
      },
      closed_by: {
        type: Sequelize.INTEGER
      },
      effective_from: {
        type: Sequelize.DATE
      },
      effective_to: {
        type: Sequelize.DATE
      },
      is_current: {
        type: Sequelize.STRING
      },
      createdAt: {
        allowNull: false,
        type: Sequelize.DATE
      },
      updatedAt: {
        allowNull: false,
        type: Sequelize.DATE
      }
    });
  },
  async down(queryInterface, Sequelize) {
    await queryInterface.dropTable('LeadVehicleDetails');
  }
};