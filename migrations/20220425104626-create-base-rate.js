'use strict';
module.exports = {
  async up(queryInterface, Sequelize) {
    await queryInterface.createTable('BaseRates', {
      id: {
        allowNull: false,
        autoIncrement: true,
        primaryKey: true,
        type: Sequelize.INTEGER
      },
      vehicleTypeId: {
        type: Sequelize.INTEGER
      },
      vehicleCategoryId: {
        type: Sequelize.INTEGER,
      },
      basePrice: {
           type: Sequelize.INTEGER
      },
      relativePrice: {
           type: Sequelize.INTEGER
      },
      created_by: {
           type: Sequelize.INTEGER
      },
      updated_by: {
           type: Sequelize.INTEGER
      },
      status: {
           type: Sequelize.INTEGER
      },
      createdAt: {
        allowNull: false,
        type: Sequelize.DATE
      },
      updatedAt: {
        allowNull: false,
        type: Sequelize.DATE
      }
    });
  },
  async down(queryInterface, Sequelize) {
    await queryInterface.dropTable('BaseRates');
  }
};