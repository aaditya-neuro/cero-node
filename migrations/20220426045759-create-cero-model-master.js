'use strict';
module.exports = {
  async up(queryInterface, Sequelize) {
    await queryInterface.createTable('cero_model_masters', {
      id: {
        allowNull: false,
        autoIncrement: true,
        primaryKey: true,
        type: Sequelize.INTEGER
      },
      rm_code: {
        type: Sequelize.STRING
      },
      make_id: {
        type: Sequelize.INTEGER
      },
      model: {
        type: Sequelize.STRING
      },
      material_desc: {
        type: Sequelize.STRING
      },
      vehicle_category_id: {
        type: Sequelize.INTEGER
      },
      uom: {
        type: Sequelize.STRING
      },
      old_material_number: {
        type: Sequelize.STRING
      },
      max_weight: {
        type: Sequelize.INTEGER
      },
      is_model_approved: {
        type: Sequelize.BOOLEAN
      },
      model_approved_by: {
        type: Sequelize.INTEGER
      },
      approved_date: {
        type: Sequelize.DATE
      },
      data_pushed_in_sap: {
        type: Sequelize.BOOLEAN
      },
      sap_response: {
        type: Sequelize.STRING
      },
      data_pushed_by: {
        type: Sequelize.INTEGER
      },
      data_pushed_date: {
        type: Sequelize.DATE
      },
      created_by: {
        type: Sequelize.INTEGER
      },
      updated_by: {
        type: Sequelize.INTEGER
      },
      createdAt: {
        allowNull: false,
        type: Sequelize.DATE
      },
      updatedAt: {
        allowNull: false,
        type: Sequelize.DATE
      }
    });
  },
  async down(queryInterface, Sequelize) {
    await queryInterface.dropTable('cero_model_masters');
  }
};