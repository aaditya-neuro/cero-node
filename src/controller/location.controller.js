//const userService = require('./user_service');
const responder = require('../utils/responder');
const locationService = require('../services/location.service');

let LocationController = {

  getCountryList: async (request, response, next) => {
    try {
    
      let countryData=await locationService.getCountryList();
    //   console.log("Data module",moduledata)
      responder.sendResponse(response, 200, "success", countryData, "Country retrieved successfully.");
    } catch (error) {
      return next(error);
    }
  },

  createCountry: async (request,response,next)=>{
    try{

      const countryData = {
        name           : request.body.countryName,
        active         : request.body.active ? request.body.active : 1
    };

    let countryCreate = await locationService.createCountry(countryData);
    responder.sendResponse(response, 200, "success", countryCreate, "Country created successfully.");
    }catch(error){
        return next(error);
    }
  },
  getByIdCountry: async (request,response,next)=>{
    try{
      const countryId = request.body.countryId;

    let countryGetById = await locationService.getByIdCountry(countryId);
    responder.sendResponse(response, 200, "success", countryGetById, "Country By Id retrieved successfully.");
    }catch(error){
        return next(error);
    }
  },
  updateCountry: async (request,response,next)=>{
    try{

      const countryData = {
        countryId           : request.body.countryId,
        countryName           : request.body.countryName
    };

    let countryUpdate = await locationService.updateCountry(countryData);
    responder.sendResponse(response, 200, "success", countryUpdate, "Country Updated successfully.");
    }catch(error){
        return next(error);
    }
  },
  deleteCountry: async (request,response,next)=>{
    try{
      const countryId = request.body.countryId;

    let countryDelete = await locationService.deleteCountry(countryId);
    responder.sendResponse(response, 200, "success", countryDelete, "Country deleted successfully.");
    }catch(error){
        return next(error);
    }
  },

  //STATE CONTROLLER


  getStateList: async (request, response, next) => {
    try {
    
      let stateData=await locationService.getStateList();
    //   console.log("Data module",moduledata)
      responder.sendResponse(response, 200, "success", stateData, "State retrieved successfully.");
    } catch (error) {
      return next(error);
    }
  },

  createState: async (request,response,next)=>{
    try{

      const stateData = {
        name           : request.body.stateName,
        countryId           : request.body.countryId,
        active         : request.body.active ? request.body.active : 1
    };

    let stateCreate = await locationService.createState(stateData);
    responder.sendResponse(response, 200, "success", stateCreate, "State created successfully.");
    }catch(error){
        return next(error);
    }
  },

  getByIdState: async (request,response,next)=>{
    try{
      const stateId = request.body.stateId;

    let stateGetById = await locationService.getByIdState(stateId);
    responder.sendResponse(response, 200, "success", stateGetById, "State By Id retrieved successfully.");
    }catch(error){
        return next(error);
    }
  },

  updateState: async (request,response,next)=>{
    try{

      const stateData = {
        stateId           : request.body.stateId,
        countryId           : request.body.countryId,
        stateName           : request.body.stateName
    };

    let stateUpdate = await locationService.updateState(stateData);
    responder.sendResponse(response, 200, "success", stateUpdate, "State Updated successfully.");
    }catch(error){
        return next(error);
    }
  },

  deleteState: async (request,response,next)=>{
    try{
      const stateId = request.body.stateId;

    let stateDelete = await locationService.deleteState(stateId);
    responder.sendResponse(response, 200, "success", stateDelete, "State deleted successfully.");
    }catch(error){
        return next(error);
    }
  },
  getByCountryState: async (request,response,next)=>{
    try{
      const countryId = request.body.countryId;

    let stateGetByCountry = await locationService.getByCountryState(countryId);
    responder.sendResponse(response, 200, "success", stateGetByCountry, "States By Country retrieved successfully.");
    }catch(error){
        return next(error);
    }
  },

  //CITY CONTROLLER 

  getCityList: async (request, response, next) => {
    try {
    
      let cityData=await locationService.getCityList();
    //   console.log("Data module",moduledata)
      responder.sendResponse(response, 200, "success", cityData, "City retrieved successfully.");
    } catch (error) {
      return next(error);
    }
  },

  createCity: async (request,response,next)=>{
    try{

      const cityData = {
        name           : request.body.cityName,
        stateId           : request.body.stateId,
        active         : request.body.active ? request.body.active : 1
    };

    let cityCreate = await locationService.createCity(cityData);
    responder.sendResponse(response, 200, "success", cityCreate, "City created successfully.");
    }catch(error){
        return next(error);
    }
  },

  getByIdCity: async (request,response,next)=>{
    try{
      const cityId = request.body.cityId;

    let cityGetById = await locationService.getByIdCity(cityId);
    responder.sendResponse(response, 200, "success", cityGetById, "City By Id retrieved successfully.");
    }catch(error){
        return next(error);
    }
  },

  getByStateCity: async (request,response,next)=>{
    try{
      const stateId = request.body.stateId;

    let cityGetByState = await locationService.getByStateCity(stateId);
    responder.sendResponse(response, 200, "success", cityGetByState, "Cities By State retrieved successfully.");
    }catch(error){
        return next(error);
    }
  },

  updateCity: async (request,response,next)=>{
    try{

      const cityData = {
        cityId           : request.body.cityId,
        stateId           : request.body.stateId,
        cityName           : request.body.cityName
    };

    let cityUpdate = await locationService.updateCity(cityData);
    responder.sendResponse(response, 200, "success", cityUpdate, "City Updated successfully.");
    }catch(error){
        return next(error);
    }
  },
  
  deleteCity: async (request,response,next)=>{
    try{
      const cityId = request.body.cityId;

    let cityDelete = await locationService.deleteCity(cityId);
    responder.sendResponse(response, 200, "success", cityDelete, "City deleted successfully.");
    }catch(error){
        return next(error);
    }
  },

  getServicableCity: async (request,response,next)=>{
    try{

    let cityList = await locationService.getServicableCity();
    responder.sendResponse(response, 200, "success", cityList, "Servicable city list retrieved successfully.");
    }catch(error){
        return next(error);
    }
  },

    //PINCODE CONTROLLER 

    getPincodeList: async (request, response, next) => {
      try {
      
        let pincodeData=await locationService.getPincodeList();
      //   console.log("Data module",moduledata)
        responder.sendResponse(response, 200, "success", pincodeData, "Pincode retrieved successfully.");
      } catch (error) {
        return next(error);
      }
    },
  
    createPincode: async (request,response,next)=>{
      try{
  
        const pincodeData = {
          name           : request.body.pincodeName,
          cityId           : request.body.cityId,
          active         : request.body.active ? request.body.active : 1
      };
  
      let pincodeCreate = await locationService.createPincode(pincodeData);
      responder.sendResponse(response, 200, "success", pincodeCreate, "Pincode created successfully.");
      }catch(error){
          return next(error);
      }
    },
  
    getByIdPincode: async (request,response,next)=>{
      try{
        const pincodeId = request.body.pincodeId;
  
      let pincodeGetById = await locationService.getByIdPincode(pincodeId);
      responder.sendResponse(response, 200, "success", pincodeGetById, "Pincode By Id retrieved successfully.");
      }catch(error){
          return next(error);
      }
    },
    getByCityPincode: async (request,response,next)=>{
      try{
        const cityId = request.body.cityId;
  
      let pincodeGetByCity = await locationService.getByCityPincode(cityId);
      responder.sendResponse(response, 200, "success", pincodeGetByCity, "Pincode By City retrieved successfully.");
      }catch(error){
          return next(error);
      }
    },
  
    updatePincode: async (request,response,next)=>{
      try{
  
        const pincodeData = {
          pincodeId           : request.body.pincodeId,
          cityId           : request.body.cityId,
          pincodeName           : request.body.pincodeName
      };
  
      let pincodeUpdate = await locationService.updatePincode(pincodeData);
      responder.sendResponse(response, 200, "success", pincodeUpdate, "Pincode Updated successfully.");
      }catch(error){
          return next(error);
      }
    },
    
    deletePincode: async (request,response,next)=>{
      try{
        const pincodeId = request.body.pincodeId;
  
      let pincodeDelete = await locationService.deletePincode(pincodeId);
      responder.sendResponse(response, 200, "success", pincodeDelete, "Pincode deleted successfully.");
      }catch(error){
          return next(error);
      }
    },

    bulkUploadPincode: async (request,response,next)=>{
      try{
        const file = request.file.filename;
  
      let pincodeBulkUpload = await locationService.bulkUploadPincode(file);
      responder.sendResponse(response, 200, "success", pincodeBulkUpload, "Pincode CSV uploaded successfully.");
      }catch(error){
          return next(error);
      }
    }

};

module.exports = LocationController;
