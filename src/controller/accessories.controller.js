const responder = require('../utils/responder');
const accessoriesService = require('../services/accessories.service');

let AccessoriesController = {

  getAccessoriesList: async (request, response, next) => {
    try {
    
      let accessoriesData=await accessoriesService.getAccessoriesList();
    //   console.log("Data module",moduledata)
      responder.sendResponse(response, 200, "success", accessoriesData, "Accessories retrieved successfully.");
    } catch (error) {
      return next(error);
    }
  },

  createAccessories: async (request,response,next)=>{
    try{

      const accessoriesData = {
        accessoriesName       : request.body.accessoriesName,
        accessoriesPrice      : request.body.accessoriesPrice,
        created_by            : 0,
        updated_by            : 0,
        status                : request.body.status ? request.body.status : 1
    };

    let accessoriesCreate = await accessoriesService.createAccessories(accessoriesData);
    responder.sendResponse(response, 200, "success", accessoriesCreate, "Accessories created successfully.");
    }catch(error){
        return next(error);
    }
  },

  updateAccessories: async (request,response,next)=>{
    try{

      const accessoriesData = {
        accessoriesId           : request.body.accessoriesId,
        accessoriesName       : request.body.accessoriesName,
        accessoriesPrice      : request.body.accessoriesPrice,
        status              : request.body.status ? request.body.status : 1
    };

    let accessoriesUpdate = await accessoriesService.updateAccessories(accessoriesData);
    responder.sendResponse(response, 200, "success", accessoriesUpdate, "Accessories Updated successfully.");
    }catch(error){
        return next(error);
    }
  },


  deleteAccessories: async (request,response,next)=>{
    try{
      const accessoriesId = request.body.accessoriesId;

    let accessoriesDelete = await accessoriesService.deleteAccessories(accessoriesId);
    responder.sendResponse(response, 200, "success", accessoriesDelete, "Accessories deleted successfully.");
    }catch(error){
        return next(error);
    }
  },


  getByIdAccessories: async (request,response,next)=>{
    try{
      const accessoriesId = request.body.accessoriesId;

    let accessoriesGetById = await accessoriesService.getByIdAccessories(accessoriesId);
    responder.sendResponse(response, 200, "success", accessoriesGetById, "Accessories By Id retrieved successfully.");
    }catch(error){
        return next(error);
    }
  },

  bulkUploadAccessories: async (request,response,next)=>{
    try{
      const file = request.file.filename;

    let accessoriesBulkUpload = await accessoriesService.bulkUploadAccessories(file);
    responder.sendResponse(response, 200, "success", accessoriesBulkUpload, "Accessories CSV uploaded successfully.");
    }catch(error){
        return next(error);
    }
  }



};

module.exports = AccessoriesController;
