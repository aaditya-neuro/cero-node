//const userService = require('./user_service');
const responder = require('../utils/responder');
const vehicleTypeService = require('../services/vehicleType.service');

let VehicleTypeController = {

  getVehicleTypeList: async (request, response, next) => {
    try {
    
      let vehicleTypeData = await vehicleTypeService.getVehicleTypeList();
    //   console.log("Data module",moduledata)
      responder.sendResponse(response, 200, "success", vehicleTypeData, "Vehicle Categories retrieved successfully.");
    } catch (error) {
      return next(error);
    }
  },

  createVehicleType: async (request,response,next)=>{
    try{

      const vehicleTypeData = {
        vehicleTypeName           : request.body.vehicleTypeName,
        status              : request.body.status ? request.body.status : 1
    };

    let vehicleTypeCreate = await vehicleTypeService.createVehicleType(vehicleTypeData);
    responder.sendResponse(response, 200, "success", vehicleTypeCreate, "Vehicle Type created successfully.");
    }catch(error){
        return next(error);
    }
  },

  updateVehicleType: async (request,response,next)=>{
    try{

      const vehicleTypeData = {
        vehicleTypeId             : request.body.vehicleTypeId,
        vehicleTypeName           : request.body.vehicleTypeName,
        status                        : request.body.status ? request.body.status : 1
    };

    let vehicleTypeUpdate = await vehicleTypeService.updateVehicleType(vehicleTypeData);
    responder.sendResponse(response, 200, "success", vehicleTypeUpdate, "Vehicle Type Updated successfully.");
    }catch(error){
        return next(error);
    }
  },


  deleteVehicleType: async (request,response,next)=>{
    try{
      const vehicleTypeId = request.body.vehicleTypeId;

    let vehicleTypeDelete = await vehicleTypeService.deleteVehicleType(vehicleTypeId);
    responder.sendResponse(response, 200, "success", vehicleTypeDelete, "Vehicle Type deleted successfully.");
    }catch(error){
        return next(error);
    }
  },


  getByIdVehicleType: async (request,response,next)=>{
    try{
      const vehicleTypeId = request.body.vehicleTypeId;

    let vehicleTypeGetById = await vehicleTypeService.getByIdVehicleType(vehicleTypeId);
    responder.sendResponse(response, 200, "success", vehicleTypeGetById, "Vehicle Type By Id retrieved successfully.");
    }catch(error){
        return next(error);
    }
  },

  bulkUploadVehicleType: async (request,response,next)=>{
    try{
      const file = request.file.filename;

    let vehicleTypeBulkUpload = await vehicleTypeService.bulkUploadVehicleType(file);
    responder.sendResponse(response, 200, "success", vehicleTypeBulkUpload, "Vehicle Type CSV uploaded successfully.");
    }catch(error){
        return next(error);
    }
  }



};

module.exports = VehicleTypeController;
