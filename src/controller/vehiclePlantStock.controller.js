const responder = require('../utils/responder');
const vehiclePlantStockService = require('../services/vehiclePlantStock.service');

let VehiclePlantStockController = {

  getVehiclePlantStockList: async (request, response, next) => {
    try {
    
      let vehiclePlantStockData=await vehiclePlantStockService.getVehiclePlantStockList();
    //   console.log("Data module",moduledata)
      responder.sendResponse(response, 200, "success", vehiclePlantStockData, "VehiclePlantStock retrieved successfully.");
    } catch (error) {
      return next(error);
    }
  },

  createVehiclePlantStock: async (request,response,next)=>{
    try{

      const vehiclePlantStockData = {
        plantId       : request.body.plantId,
        rmCode      : request.body.rmCode,
        stockLevel      : request.body.stockLevel,
        demand      : request.body.demand,
        created_by            : 0,
        updated_by            : 0,
        status                : request.body.status ? request.body.status : 1
    };

    let vehiclePlantStockCreate = await vehiclePlantStockService.createVehiclePlantStock(vehiclePlantStockData);
    responder.sendResponse(response, 200, "success", vehiclePlantStockCreate, "VehiclePlantStock created successfully.");
    }catch(error){
        return next(error);
    }
  },

  updateVehiclePlantStock: async (request,response,next)=>{
    try{

      const vehiclePlantStockData = {
        vehiclePlantStockId       : request.body.vehiclePlantStockId,
        plantId                   : request.body.plantId,
        rmCode                    : request.body.rmCode,
        stockLevel                : request.body.stockLevel,
        demand                    : request.body.demand,
        status                    : request.body.status ? request.body.status : 1
    };

    let vehiclePlantStockUpdate = await vehiclePlantStockService.updateVehiclePlantStock(vehiclePlantStockData);
    responder.sendResponse(response, 200, "success", vehiclePlantStockUpdate, "VehiclePlantStock Updated successfully.");
    }catch(error){
        return next(error);
    }
  },


  deleteVehiclePlantStock: async (request,response,next)=>{
    try{
      const vehiclePlantStockId = request.body.vehiclePlantStockId;

    let vehiclePlantStockDelete = await vehiclePlantStockService.deleteVehiclePlantStock(vehiclePlantStockId);
    responder.sendResponse(response, 200, "success", vehiclePlantStockDelete, "VehiclePlantStock deleted successfully.");
    }catch(error){
        return next(error);
    }
  },


  getByIdVehiclePlantStock: async (request,response,next)=>{
    try{
      const vehiclePlantStockId = request.body.vehiclePlantStockId;

    let vehiclePlantStockGetById = await vehiclePlantStockService.getByIdVehiclePlantStock(vehiclePlantStockId);
    responder.sendResponse(response, 200, "success", vehiclePlantStockGetById, "VehiclePlantStock By Id retrieved successfully.");
    }catch(error){
        return next(error);
    }
  },

  bulkUploadVehiclePlantStock: async (request,response,next)=>{
    try{
      const file = request.file.filename;

    let vehiclePlantStockBulkUpload = await vehiclePlantStockService.bulkUploadVehiclePlantStock(file);
    responder.sendResponse(response, 200, "success", vehiclePlantStockBulkUpload, "VehiclePlantStock CSV uploaded successfully.");
    }catch(error){
        return next(error);
    }
  }



};

module.exports = VehiclePlantStockController;
