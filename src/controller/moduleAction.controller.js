//const userService = require('./user_service');
const responder = require('../utils/responder');
const moduleActionService = require('../services/moduleAction.sevice');

let ModuleController = {

    getModuleActions: async (request, response, next) => {
    try {
    
      let moduledata=await moduleActionService.getModuleActions();
      //console.log("Data module",moduledata)
      responder.sendResponse(response, 200, "success", moduledata, "Module retrieved successfully.");
    } catch (error) {
      return next(error);
    }
  },

//   getUser: async (request, response, next) => {
//     try {
//       let dbKey = await common.getDBKeyFromRequest(request);
//       let userId = request.params.userId;
//       let user = await userService.getUser(userId, dbKey);
//       responder.sendResponse(response, 200, "success", user, "User retrieved successfully.");
//     } catch (error) {
//       return next(error);
//     }
//   },

  createModuleAction: async (request, response, next) => {
    try {
     
      //let body = request.body;
      const moduleAcdata = {
        moduleId    : request.body.moduleId,
        actionName  : request.body.actionName,
        actionLevel : request.body.actionLevel,
        sequence    : request.body.sequence,
        status      : request.body.status ? request.body.status : 1,
        isDeleted   : request.body.status ? request.body.isDeleted : 0,
        icon        : request.body.icon ? request.body.icon : ''
    };
      let moduleCreate = await moduleActionService.createModuleAction(moduleAcdata);
      responder.sendResponse(response, 200, "success", moduleCreate, "Module created successfully.");
    } catch (error) {
      return next(error);
    }
  },


  moduleMappedActions: async (request, response, next) => {
    try {
     
      let moduleId  =  request.body.moduleId;
      let module_action = await moduleService.moduleMappedActions(moduleId);
      responder.sendResponse(response, 200, "success", module_action, "Particular Module Mapped Action List.");
    } catch (error) {
      return next(error);
    }
  },

//   updateUser: async (request, response, next) => {
//     try {
//       let dbKey = await common.getDBKeyFromRequest(request);
//       let user = await userService.updateUser(dbKey);
//       response.status(200).json({
//         status: "success",
//         data: user,
//         message: "User updated successfully."
//       });
//     } catch (error) {
//       return next(error);
//     }
//   },

//   deleteUser: async (request, response, next) => {
//     try {
//       let dbKey = await common.getDBKeyFromRequest(request);
//       let userId = request.params.userId;
//       let user = await userService.deleteUser(userId, dbKey);
//       responder.sendResponse(response, 200, "success", user, "User deleted successfully.");
//     } catch (error) {
//       return next(error);
//     }
//   }

};

module.exports = ModuleController;
