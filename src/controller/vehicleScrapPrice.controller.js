//const userService = require('./user_service');
const responder = require('../utils/responder');
const vehicleScrapPriceService = require('../services/vehicleScrapPrice.service');

let VehicleScrapPriceController = {

  getVehicleScrapPriceList: async (request, response, next) => {
    try {
    
      let vehicleScrapPriceData = await vehicleScrapPriceService.getVehicleScrapPriceList();
    //   console.log("Data module",moduledata)
      responder.sendResponse(response, 200, "success", vehicleScrapPriceData, "Vehicle Scrap Prices retrieved successfully.");
    } catch (error) {
      return next(error);
    }
  },

  createVehicleScrapPrice: async (request,response,next)=>{
    try{

      const vehicleScrapPriceData = {
        vehicleCategoryId   : request.body.vehicleCategoryId,
        vehicleTypeId       : request.body.vehicleTypeId,
        vehicleScrapBasePrice           : request.body.vehicleScrapBasePrice,
        status              : request.body.status ? request.body.status : 1
    };

    let vehicleScrapPriceCreate = await vehicleScrapPriceService.createVehicleScrapPrice(vehicleScrapPriceData);
    responder.sendResponse(response, 200, "success", vehicleScrapPriceCreate, "Vehicle ScrapPrice created successfully.");
    }catch(error){
        return next(error);
    }
  },

  updateVehicleScrapPrice: async (request,response,next)=>{
    try{

      const vehicleScrapPriceData = {
        vehicleScrapPriceId             : request.body.vehicleScrapPriceId,
        vehicleCategoryId   : request.body.vehicleCategoryId,
        vehicleTypeId       : request.body.vehicleTypeId,
        vehicleScrapBasePrice           : request.body.vehicleScrapBasePrice,
        status                        : request.body.status ? request.body.status : 1
    };

    let vehicleScrapPriceUpdate = await vehicleScrapPriceService.updateVehicleScrapPrice(vehicleScrapPriceData);
    responder.sendResponse(response, 200, "success", vehicleScrapPriceUpdate, "Vehicle ScrapPrice Updated successfully.");
    }catch(error){
        return next(error);
    }
  },


  deleteVehicleScrapPrice: async (request,response,next)=>{
    try{
      const vehicleScrapPriceId = request.body.vehicleScrapPriceId;

    let vehicleScrapPriceDelete = await vehicleScrapPriceService.deleteVehicleScrapPrice(vehicleScrapPriceId);
    responder.sendResponse(response, 200, "success", vehicleScrapPriceDelete, "Vehicle ScrapPrice deleted successfully.");
    }catch(error){
        return next(error);
    }
  },


  getByIdVehicleScrapPrice: async (request,response,next)=>{
    try{
      const vehicleScrapPriceId = request.body.vehicleScrapPriceId;

    let vehicleScrapPriceGetById = await vehicleScrapPriceService.getByIdVehicleScrapPrice(vehicleScrapPriceId);
    responder.sendResponse(response, 200, "success", vehicleScrapPriceGetById, "Vehicle ScrapPrice By Id retrieved successfully.");
    }catch(error){
        return next(error);
    }
  },

  bulkUploadVehicleScrapPrice: async (request,response,next)=>{
    try{
      const file = request.file.filename;

    let vehicleScrapPriceBulkUpload = await vehicleScrapPriceService.bulkUploadVehicleScrapPrice(file);
    responder.sendResponse(response, 200, "success", vehicleScrapPriceBulkUpload, "Vehicle ScrapPrice CSV uploaded successfully.");
    }catch(error){
        return next(error);
    }
  }



};

module.exports = VehicleScrapPriceController;
