//const userService = require('./user_service');
const responder = require('../utils/responder');
const plantService = require('../services/plant.service');

let PlantController = {

  getPlantList: async (request, response, next) => {
    try {
    
      let plantData=await plantService.getPlantList();
    //   console.log("Data module",moduledata)
      responder.sendResponse(response, 200, "success", plantData, "Plants retrieved successfully.");
    } catch (error) {
      return next(error);
    }
  },

  createPlant: async (request,response,next)=>{
    try{

      const plantData = {
        plant_name           : request.body.plantName,
        state               : request.body.state,
        city                : request.body.city,
        address             : request.body.address,
        serviceable_city     : request.body.serviceableCity,
        Created_by           : request.body.CreatedBy,
        updated_by           : request.body.updatedBy,
        status              : request.body.status ? request.body.status : 1
    };

    let plantCreate = await plantService.createPlant(plantData);
    responder.sendResponse(response, 200, "success", plantCreate, "Plant created successfully.");
    }catch(error){
        return next(error);
    }
  },

  updatePlant: async (request,response,next)=>{
    try{

      const plantData = {
        plantId           : request.body.plantId,
        plantName           : request.body.plantName,
        state               : request.body.state,
        city                : request.body.city,
        address             : request.body.address,
        serviceableCity     : request.body.serviceableCity,
        CreatedBy           : request.body.CreatedBy,
        updatedBy           : request.body.updatedBy,
        status              : request.body.status ? request.body.status : 1
    };

    let plantUpdate = await plantService.updatePlant(plantData);
    responder.sendResponse(response, 200, "success", plantUpdate, "Plant Updated successfully.");
    }catch(error){
        return next(error);
    }
  },


  deletePlant: async (request,response,next)=>{
    try{
      const plantId = request.body.plantId;

    let plantDelete = await plantService.deletePlant(plantId);
    responder.sendResponse(response, 200, "success", plantDelete, "Plant deleted successfully.");
    }catch(error){
        return next(error);
    }
  },


  getByIdPlant: async (request,response,next)=>{
    try{
      const plantId = request.body.plantId;

    let plantGetById = await plantService.getByIdPlant(plantId);
    responder.sendResponse(response, 200, "success", plantGetById, "Plant By Id retrieved successfully.");
    }catch(error){
        return next(error);
    }
  },

  bulkUploadPlant: async (request,response,next)=>{
    try{
      const file = request.file.filename;

    let plantBulkUpload = await plantService.bulkUploadPlant(file);
    responder.sendResponse(response, 200, "success", plantBulkUpload, "Plant CSV uploaded successfully.");
    }catch(error){
        return next(error);
    }
  }



};

module.exports = PlantController;
