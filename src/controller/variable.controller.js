const responder = require('../utils/responder');
const variableService = require('../services/variable.service');

let VariableController = {

  getVariableList: async (request, response, next) => {
    try {
    
      let variableData=await variableService.getVariableList();
    //   console.log("Data module",moduledata)
      responder.sendResponse(response, 200, "success", variableData, "Variable retrieved successfully.");
    } catch (error) {
      return next(error);
    }
  },

  createVariable: async (request,response,next)=>{
    try{

      const variableData = {
        variableType       : request.body.variableType,
        variableValue       : request.body.variableValue,
        price                 : request.body.price,
        created_by            : 0,
        updated_by            : 0,
        status                : request.body.status ? request.body.status : 1
    };

    let variableCreate = await variableService.createVariable(variableData);
    responder.sendResponse(response, 200, "success", variableCreate, "Variable created successfully.");
    }catch(error){
        return next(error);
    }
  },

  updateVariable: async (request,response,next)=>{
    try{

      const variableData = {
        variableId           : request.body.variableId,
        variableType       : request.body.variableType,
        variableValue       : request.body.variableValue,
        price                 : request.body.price,
        status              : request.body.status ? request.body.status : 1
    };

    let variableUpdate = await variableService.updateVariable(variableData);
    responder.sendResponse(response, 200, "success", variableUpdate, "Variable Updated successfully.");
    }catch(error){
        return next(error);
    }
  },


  deleteVariable: async (request,response,next)=>{
    try{
      const variableId = request.body.variableId;

    let variableDelete = await variableService.deleteVariable(variableId);
    responder.sendResponse(response, 200, "success", variableDelete, "Variable deleted successfully.");
    }catch(error){
        return next(error);
    }
  },


  getByIdVariable: async (request,response,next)=>{
    try{
      const variableId = request.body.variableId;

    let variableGetById = await variableService.getByIdVariable(variableId);
    responder.sendResponse(response, 200, "success", variableGetById, "Variable By Id retrieved successfully.");
    }catch(error){
        return next(error);
    }
  },

  bulkUploadVariable: async (request,response,next)=>{
    try{
      const file = request.file.filename;

    let variableBulkUpload = await variableService.bulkUploadVariable(file);
    responder.sendResponse(response, 200, "success", variableBulkUpload, "Variable CSV uploaded successfully.");
    }catch(error){
        return next(error);
    }
  }



};

module.exports = VariableController;
