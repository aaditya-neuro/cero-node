//const userService = require('./user_service');
const responder = require('../utils/responder');
const makeModelService = require('../services/makemodel.service');

function format (number) {
  let [ integer, fraction = '' ] = number.toString().split('.')
  let sign = ''
  if (integer.startsWith('-')) {
      integer = integer.slice(1)
      sign = '-'
  }
  integer = integer.padStart(4, '0')
  if (fraction) {
      fraction = '.' + fraction.padEnd(6, '0')
  }
  let string = sign + integer + fraction
  return string
}

function incrementAlphanumeric(prefix,latestnum) {
  var arr = latestnum.split(prefix);
  var num = parseInt(arr[1]);
  arr.splice(-1, 1);
  // var str = arr.join();  
  num++;       
  console.log("STR "+prefix+" num "+num)
  latestnum = prefix + format(num);    
  return latestnum;
  
}

let MakeModelController = {

  getMakeList: async (request, response, next) => {
    try {
    
      let makeData=await makeModelService.getMakeList();
    //   console.log("Data module",moduledata)
      responder.sendResponse(response, 200, "success", makeData, "Make Data retrieved successfully.");
    } catch (error) {
      return next(error);
    }
  },

  createMake: async (request,response,next)=>{
    try{

      const makeData = {
        make           : request.body.makeName,
        make_sap_code              : request.body.makeSAPCode,
        vehicle_category_id  : request.body.vehicleCategoryId,
        status              : request.body.status?request.body.status:0,
        created_by           : 0,
        updated_by           : 0,
    };

    let makeCreate = await makeModelService.createMake(makeData);
    responder.sendResponse(response, 200, "success", makeCreate, "Make created successfully.");
    }catch(error){
        return next(error);
    }
  },

  updateMake: async (request,response,next)=>{
    try{

      const makeData = {
        makeId:   request.body.makeId,
        make           : request.body.makeName,
        make_sap_code  : request.body.makeSAPCode,
        vehicle_category_id  : request.body.vehicleCategoryId,
        status              : request.body.status?request.body.status:0,
        created_by           : 0,
        updated_by           : 0,
    };

    let makeUpdate = await makeModelService.updateMake(makeData);
    responder.sendResponse(response, 200, "success", makeUpdate, "Make Updated successfully.");
    }catch(error){
        return next(error);
    }
  },

  deleteMake: async (request,response,next)=>{
    try{
      const makeId = request.body.makeId;

    let makeDelete = await makeModelService.deleteMake(makeId);
    responder.sendResponse(response, 200, "success", makeDelete, "Make deleted successfully.");
    }catch(error){
        return next(error);
    }
  },

  getByIdMake: async (request,response,next)=>{
    try{
      const makeId = request.body.makeId;

    let makeGetById = await makeModelService.getByIdMake(makeId);
    responder.sendResponse(response, 200, "success", makeGetById, "Make By Id retrieved successfully.");
    }catch(error){
        return next(error);
    }
  },

// Model CURD

  getModelList: async (request, response, next) => {
    try {
    
      let modelData=await makeModelService.getModelList();
      responder.sendResponse(response, 200, "success", modelData, "Model Data retrieved successfully.");
    } catch (error) {
      return next(error);
    }
  },

  createModel: async (request,response,next)=>{
    try{

        let makeCode = await makeModelService.getMakeCode(request.body.makeId);

        let vehicleCatagoryCode =  await makeModelService.getVehicleCatCodeByMake(request.body.vehicleCategoryId);

        let prefix =  "RA"+vehicleCatagoryCode+makeCode;

        let latestnum = await makeModelService.getLatestRMCode(prefix);
        console.log("latestnum",latestnum)

        let rmCode = await incrementAlphanumeric(prefix,latestnum);
        console.log("rmCode",rmCode)        

        const modelData = {
          make_id                 : request.body.makeId,
          max_weight              : request.body.maxWeight,
          rm_code                 : rmCode,
          uom                     : request.body.uom,
          old_material_number     : request.body.oldMaterialNumber,
          model                   : request.body.model,
          material_desc           : request.body.materialDesc,
          vehicle_category_id     : request.body.vehicleCategoryId,
          created_by              : 0,
          updated_by              : 0,
      };

      let modelCreate = await makeModelService.createModel(modelData);
      responder.sendResponse(response, 200, "success", modelCreate, "Model created successfully.");
    }catch(error){
        return next(error);
    }
  },

  getByIdModel: async (request,response,next)=>{
    try{
      const modelId = request.body.modelId;

    let modelGetById = await makeModelService.getByIdModel(modelId);
    responder.sendResponse(response, 200, "success", modelGetById, "Model By Id retrieved successfully.");
    }catch(error){
        return next(error);
    }
  },

  updateModel: async (request,response,next)=>{
    try{

      const modelData = {
        modelId:   request.body.modelId,
        make_id           : request.body.makeId,
        max_weight              : request.body.maxWeight,
        uom              : request.body.uom,
        old_material_number              : request.body.oldMaterialNumber,
        model              : request.body.model,
        material_desc              : request.body.materialDesc,
        vehicle_category_id              : request.body.vehicleCategoryId,
        created_by           : 0,
        updated_by           : 0,
    };

    let modelUpdate = await makeModelService.updateModel(modelData);
    responder.sendResponse(response, 200, "success", modelUpdate, "Model Updated successfully.");
    }catch(error){
        return next(error);
    }
  },

  deleteModel: async (request,response,next)=>{
    try{
      const modelId = request.body.modelId;

    let modelDelete = await makeModelService.deleteModel(modelId);
    responder.sendResponse(response, 200, "success", modelDelete, "Model deleted successfully.");
    }catch(error){
        return next(error);
    }
  },



};

module.exports = MakeModelController;
