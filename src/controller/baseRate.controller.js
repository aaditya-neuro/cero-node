const responder = require('../utils/responder');
const baseRateService = require('../services/baseRate.service');

let BaseRateController = {

  getBaseRateList: async (request, response, next) => {
    try {
    
      let baseRateData=await baseRateService.getBaseRateList();
    //   console.log("Data module",moduledata)
      responder.sendResponse(response, 200, "success", baseRateData, "baseRate retrieved successfully.");
    } catch (error) {
      return next(error);
    }
  },

  createBaseRate: async (request,response,next)=>{
    try{

      const baseRateData = {
        vehicleTypeId       : request.body.vehicleTypeId,
        vehicleCategoryId      : request.body.vehicleCategoryId,
        basePrice      : request.body.basePrice,
        relativePrice      : request.body.relativePrice,
        created_by            : 0,
        updated_by            : 0,
        status                : request.body.status ? request.body.status : 1
    };

    let baseRateCreate = await baseRateService.createBaseRate(baseRateData);
    responder.sendResponse(response, 200, "success", baseRateCreate, "BaseRate created successfully.");
    }catch(error){
        return next(error);
    }
  },

  updateBaseRate: async (request,response,next)=>{
    try{

      const baseRateData = {
        baseRateId           : request.body.baseRateId,
        vehicleTypeId       : request.body.vehicleTypeId,
        vehicleCategoryId      : request.body.vehicleCategoryId,
        basePrice      : request.body.basePrice,
        relativePrice      : request.body.relativePrice,
        status              : request.body.status ? request.body.status : 1
    };

    let baseRateUpdate = await baseRateService.updateBaseRate(baseRateData);
    responder.sendResponse(response, 200, "success", baseRateUpdate, "BaseRate Updated successfully.");
    }catch(error){
        return next(error);
    }
  },


  deleteBaseRate: async (request,response,next)=>{
    try{
      const baseRateId = request.body.baseRateId;

    let baseRateDelete = await baseRateService.deleteBaseRate(baseRateId);
    responder.sendResponse(response, 200, "success", baseRateDelete, "BaseRate deleted successfully.");
    }catch(error){
        return next(error);
    }
  },


  getByIdBaseRate: async (request,response,next)=>{
    try{
      const baseRateId = request.body.baseRateId;

    let baseRateGetById = await baseRateService.getByIdBaseRate(baseRateId);
    responder.sendResponse(response, 200, "success", baseRateGetById, "BaseRate By Id retrieved successfully.");
    }catch(error){
        return next(error);
    }
  },

  bulkUploadBaseRate: async (request,response,next)=>{
    try{
      const file = request.file.filename;

    let baseRateBulkUpload = await baseRateService.bulkUploadBaseRate(file);
    responder.sendResponse(response, 200, "success", baseRateBulkUpload, "BaseRate CSV uploaded successfully.");
    }catch(error){
        return next(error);
    }
  }



};

module.exports = BaseRateController;
