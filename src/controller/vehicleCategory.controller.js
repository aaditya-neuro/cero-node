//const userService = require('./user_service');
const responder = require('../utils/responder');
const vehicleCategoryService = require('../services/vehicleCategory.service');

let VehicleCategoryController = {

  getVehicleCategoryList: async (request, response, next) => {
    try {
    
      let vehicleCategoryData = await vehicleCategoryService.getVehicleCategoryList();
    //   console.log("Data module",moduledata)
      responder.sendResponse(response, 200, "success", vehicleCategoryData, "Vehicle Categories retrieved successfully.");
    } catch (error) {
      return next(error);
    }
  },

  createVehicleCategory: async (request,response,next)=>{
    try{

      const vehicleCategoryData = {
        vehicleCategoryName           : request.body.vehicleCategoryName,
        vehicleCategoryCode           : request.body.vehicleCategoryCode,
        vehicleCategoryalfa           : request.body.vehicleCategoryalfa,
        status              : request.body.status ? request.body.status : 1
    };

    let vehicleCategoryCreate = await vehicleCategoryService.createVehicleCategory(vehicleCategoryData);
    responder.sendResponse(response, 200, "success", vehicleCategoryCreate, "Vehicle Category created successfully.");
    }catch(error){
        return next(error);
    }
  },

  updateVehicleCategory: async (request,response,next)=>{
    try{

      const vehicleCategoryData = {
        vehicleCategoryId             : request.body.vehicleCategoryId,
        vehicleCategoryName           : request.body.vehicleCategoryName,
        vehicleCategoryCode           : request.body.vehicleCategoryCode,
        vehicleCategoryalfa           : request.body.vehicleCategoryalfa,
        status                        : request.body.status ? request.body.status : 1
    };

    let vehicleCategoryUpdate = await vehicleCategoryService.updateVehicleCategory(vehicleCategoryData);
    responder.sendResponse(response, 200, "success", vehicleCategoryUpdate, "Vehicle Category Updated successfully.");
    }catch(error){
        return next(error);
    }
  },


  deleteVehicleCategory: async (request,response,next)=>{
    try{
      const vehicleCategoryId = request.body.vehicleCategoryId;

    let vehicleCategoryDelete = await vehicleCategoryService.deleteVehicleCategory(vehicleCategoryId);
    responder.sendResponse(response, 200, "success", vehicleCategoryDelete, "Vehicle Category deleted successfully.");
    }catch(error){
        return next(error);
    }
  },


  getByIdVehicleCategory: async (request,response,next)=>{
    try{
      const vehicleCategoryId = request.body.vehicleCategoryId;

    let vehicleCategoryGetById = await vehicleCategoryService.getByIdVehicleCategory(vehicleCategoryId);
    responder.sendResponse(response, 200, "success", vehicleCategoryGetById, "Vehicle Category By Id retrieved successfully.");
    }catch(error){
        return next(error);
    }
  },

  bulkUploadVehicleCategory: async (request,response,next)=>{
    try{
      const file = request.file.filename;

    let vehicleCategoryBulkUpload = await vehicleCategoryService.bulkUploadVehicleCategory(file);
    responder.sendResponse(response, 200, "success", vehicleCategoryBulkUpload, "Vehicle Category CSV uploaded successfully.");
    }catch(error){
        return next(error);
    }
  }



};

module.exports = VehicleCategoryController;
