const Joi = require('joi') 
const schemas = { 
  vehicleCategoryCreate: Joi.object({
    vehicleCategoryName: Joi.string().regex(/^[a-z0-9]+(?:-[a-z0-9]+)*$/).lowercase().required()
                .messages({
                         'string.pattern.base': `vehicleCategoryName is only alphanumaric`
                       }),
    vehicleCategoryCode: Joi.string().regex(/^[a-z0-9]+(?:-[a-z0-9]+)*$/).lowercase().required()
    .messages({
            'string.pattern.base': `vehicleCategoryCode is only alphanumaric`
          }),
    vehicleCategoryalfa: Joi.string().regex(/^[a-z0-9]+(?:-[a-z0-9]+)*$/).lowercase().required()
    .messages({
            'string.pattern.base': `vehicleCategoryalfa is only alphanumaric`
          }),                        
    status: Joi.number().valid(1, 0).required(), 
  }),
  vehicleCategoryUpdate: Joi.object({
    vehicleCategoryId:  Joi.number().required(), 
    vehicleCategoryName: Joi.string().regex(/^[a-z0-9]+(?:-[a-z0-9]+)*$/).lowercase().required()
                .messages({
                         'string.pattern.base': `vehicleCategoryName is only alphanumaric`
                       }),
    status: Joi.number().valid(1, 0).required(), 
  }),
  vehicleCategoryCode: Joi.string().regex(/^[a-z0-9]+(?:-[a-z0-9]+)*$/).lowercase().required()
    .messages({
            'string.pattern.base': `vehicleCategoryCode is only alphanumaric`
          }),
    vehicleCategoryalfa: Joi.string().regex(/^[a-z0-9]+(?:-[a-z0-9]+)*$/).lowercase().required()
    .messages({
            'string.pattern.base': `vehicleCategoryalfa is only alphanumaric`
          }),     
  vehicleCategoryDelete: Joi.object({
    vehicleCategoryId:  Joi.number().required() 
  }),
  vehicleCategoryGetById: Joi.object({
    vehicleCategoryId:  Joi.number().required() 
  }),
  
  // define all the other schemas below 
}; 
module.exports = schemas;