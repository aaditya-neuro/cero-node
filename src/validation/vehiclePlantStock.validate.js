const Joi = require('joi') 
const schemas = { 
  vehiclePlantStockCreate: Joi.object({
      rmCode: Joi.string().regex(/^[a-z0-9]+(?:-[a-z0-9]+)*$/).lowercase().required()
                .messages({
                         'string.pattern.base': `rmCode is only alphanumaric`
                       }),
      plantId: Joi.number().required(),
      stockLevel: Joi.number().required(),
      demand: Joi.number().required(),

    status: Joi.number().valid(1, 0).required(), 
  }),
  vehiclePlantStockUpdate: Joi.object({
    vehiclePlantStockId:  Joi.number().required(), 
    rmCode: Joi.string().regex(/^[a-z0-9]+(?:-[a-z0-9]+)*$/).lowercase().required()
                .messages({
                         'string.pattern.base': `rmCode is only alphanumaric`
                       }),
      plantId: Joi.number().required(),
      stockLevel: Joi.number().required(),
      demand: Joi.number().required(),
    status: Joi.number().valid(1, 0).required(), 

  }),
  vehiclePlantStockDelete: Joi.object({
    vehiclePlantStockId:  Joi.number().required() 
  }),
  vehiclePlantStockGetById: Joi.object({
    vehiclePlantStockId:  Joi.number().required() 
  }),
  
  // define all the other schemas below 
}; 
module.exports = schemas;