const Joi = require('joi') 
const schemas = { 
  moduleCreate: Joi.object({
    moduleName: Joi.string().regex(/^[a-z0-9]+(?:-[a-z0-9]+)*$/).lowercase().required()
                .messages({
                         'string.pattern.base': `moduleName is only alphabets numebers and -`
                       }), 
    moduleLevel: Joi.string()
                 .regex(/^[a-zA-Z ]*$/)
                 .required()
                 .messages({
                    'string.pattern.base': `moduleLevel is accept  only alphabets and space`
                  }),
    sequence: Joi.number().required(), 
    status: Joi.number().valid(1, 0).required(), 
    isDeleted: Joi.number().valid(1, 0).optional(),
    icon: Joi.string().allow('').optional()
  }),
  moduleMappedActions: Joi.object({
      moduleId: Joi.number().integer().required(), 
   }),
  

  // define all the other schemas below 
}; 
module.exports = schemas;