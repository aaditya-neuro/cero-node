const Joi = require('joi') 
const schemas = { 

///COUNTRY VALIDATIONS
 countryCreate: Joi.object({
    countryName: Joi.string().regex(/^[a-z0-9]+(?:-[a-z0-9]+)*$/).lowercase().required()
                .messages({
                         'string.pattern.base': `Name is only alphanumaric`
                       }),
  }),
  countryUpdate: Joi.object({
    countryId:  Joi.number().required(), 
    countryName: Joi.string().regex(/^[a-z0-9]+(?:-[a-z0-9]+)*$/).lowercase().required()
                .messages({
                         'string.pattern.base': `Name is only alphanumaric`
                       }),
  }),
  countryGetById: Joi.object({
    countryId:  Joi.number().required() 
  }),
  countryDelete: Joi.object({
    countryId:  Joi.number().required() 
  }),

///STATE VALIDATIONS
  stateCreate: Joi.object({
    countryId:  Joi.number().required() ,
    stateName: Joi.string().regex(/^[a-z0-9]+(?:-[a-z0-9]+)*$/).lowercase().required()
                .messages({
                         'string.pattern.base': `Name is only alphanumaric`
                       }),
  }),
  stateUpdate: Joi.object({
    stateId:  Joi.number().required(), 
    countryId:  Joi.number().required(),
    stateName: Joi.string().regex(/^[a-z0-9]+(?:-[a-z0-9]+)*$/).lowercase().required()
                .messages({
                         'string.pattern.base': `Name is only alphanumaric`
                       }),
  }),
  stateGetById: Joi.object({
    stateId:  Joi.number().required() 
  }),
  stateDelete: Joi.object({
    stateId:  Joi.number().required() 
  }),

  stateGetByCountry: Joi.object({
    countryId:  Joi.number().required() 
  }),

  ///CITY VALIDATIONS
  cityCreate: Joi.object({
    stateId:  Joi.number().required() ,
    cityName: Joi.string().regex(/^[a-z0-9]+(?:-[a-z0-9]+)*$/).lowercase().required()
                .messages({
                        'string.pattern.base': `Name is only alphanumaric`
                      }),
  }),
  cityUpdate: Joi.object({
    cityId:  Joi.number().required(), 
    stateId:  Joi.number().required(),
    cityName: Joi.string().regex(/^[a-z0-9]+(?:-[a-z0-9]+)*$/).lowercase().required()
                .messages({
                        'string.pattern.base': `Name is only alphanumaric`
                      }),
  }),
  cityGetById: Joi.object({
    cityId:  Joi.number().required() 
  }),
  cityDelete: Joi.object({
    cityId:  Joi.number().required() 
  }),
  cityGetByState: Joi.object({
    stateId:  Joi.number().required() 
  }),

  ///PINCODE VALIDATIONS
  pincodeCreate: Joi.object({
    cityId:  Joi.number().required() ,
    pincodeName: Joi.string().regex(/^[a-z0-9]+(?:-[a-z0-9]+)*$/).lowercase().required()
                .messages({
                        'string.pattern.base': `Name is only alphanumaric`
                      }),
  }),
  pincodeUpdate: Joi.object({
    pincodeId:  Joi.number().required(), 
    cityId:  Joi.number().required(),
    pincodeName: Joi.string().regex(/^[a-z0-9]+(?:-[a-z0-9]+)*$/).lowercase().required()
                .messages({
                        'string.pattern.base': `Name is only alphanumaric`
                      }),
  }),
  pincodeGetById: Joi.object({
    pincodeId:  Joi.number().required() 
  }),
  pincodeGetByCity: Joi.object({
    cityId:  Joi.number().required() 
  }),
  pincodeDelete: Joi.object({
    pincodeId:  Joi.number().required() 
  }),

  // define all the other schemas below 
}; 
module.exports = schemas;