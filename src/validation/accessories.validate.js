const Joi = require('joi') 
const schemas = { 
    accessoriesCreate: Joi.object({
    accessoriesName: Joi.string().regex(/^[ A-Za-z0-9_@./#&+-]*$/).lowercase().required()
                .messages({
                         'string.pattern.base': `accessoriesName is only alphanumaric`
                       }),
    accessoriesPrice: Joi.number().required(),
    status: Joi.number().valid(1, 0).required(), 
  }),
  accessoriesUpdate: Joi.object({
    accessoriesId:  Joi.number().required(), 
    accessoriesName: Joi.string().regex(/^[a-z0-9]+(?:-[a-z0-9]+)*$/).lowercase().required()
                .messages({
                         'string.pattern.base': `AccessoriesName is only alphanumaric`
                       }),
    accessoriesPrice: Joi.number().required(),
    status: Joi.number().valid(1, 0).required(), 
  }),
  accessoriesDelete: Joi.object({
    accessoriesId:  Joi.number().required() 
  }),
  accessoriesGetById: Joi.object({
    accessoriesId:  Joi.number().required() 
  }),
  
  // define all the other schemas below 
}; 
module.exports = schemas;