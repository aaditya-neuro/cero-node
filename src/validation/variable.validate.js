const Joi = require('joi') 
const schemas = { 
    variableCreate: Joi.object({
        variableType: Joi.string().regex(/^[ A-Za-z0-9_@./#&+-]*$/).lowercase().required()
                .messages({
                         'string.pattern.base': `variableType is only alphanumaric`
                       }),
        variableValue: Joi.number().required(),
        price: Joi.number().required(),
        status: Joi.number().valid(1, 0).required(), 
  }),
  variableUpdate: Joi.object({
    variableId:  Joi.number().required(), 
    variableType: Joi.string().regex(/^[ A-Za-z0-9_@./#&+-]*$/).lowercase().required()
        .messages({
                'string.pattern.base': `variableType is only alphanumaric`
            }),
    variableValue: Joi.number().required(),
    price: Joi.number().required(),
    status: Joi.number().valid(1, 0).required(), 
  }),
  variableDelete: Joi.object({
    variableId:  Joi.number().required() 
  }),
  variableGetById: Joi.object({
    variableId:  Joi.number().required() 
  }),
  
  // define all the other schemas below 
}; 
module.exports = schemas;