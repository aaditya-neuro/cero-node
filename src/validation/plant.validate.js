const Joi = require('joi') 
const schemas = { 
  plantCreate: Joi.object({
    plantName: Joi.string().regex(/^[a-z0-9]+(?:-[a-z0-9]+)*$/).lowercase().required()
                .messages({
                         'string.pattern.base': `plantName is only alphanumaric`
                       }),
    state: Joi.number().required(), 
    city: Joi.number().required(), 
    address:Joi.string().regex(/^[ A-Za-z0-9_]*$/).lowercase().required()
    .messages({
             'string.pattern.base': `Address is only alphanumaric`
           }), 
    serviceableCity: Joi.array().items(Joi.number()), 
    CreatedBy: Joi.number().required(), 
    updatedBy: Joi.number().required(), 
    status: Joi.number().valid(1, 0).required(), 
  }),
  plantUpdate: Joi.object({
    plantId:  Joi.number().required(), 
    plantName: Joi.string().regex(/^[a-z0-9]+(?:-[a-z0-9]+)*$/).lowercase().required()
                .messages({
                         'string.pattern.base': `plantName is only alphanumaric`
                       }),
    state: Joi.number().required(), 
    city: Joi.number().required(), 
    address:Joi.string().regex(/^[ A-Za-z0-9_]*$/).lowercase().required()
    .messages({
             'string.pattern.base': `Address is only alphanumaric`
           }), 
    serviceableCity: Joi.array().items(Joi.number()), 
    CreatedBy: Joi.number().required(), 
    updatedBy: Joi.number().required(), 
    status: Joi.number().valid(1, 0).required(), 
  }),
  plantDelete: Joi.object({
    plantId:  Joi.number().required() 
  }),
  plantGetById: Joi.object({
    plantId:  Joi.number().required() 
  }),
  
  // define all the other schemas below 
}; 
module.exports = schemas;