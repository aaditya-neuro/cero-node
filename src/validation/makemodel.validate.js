const Joi = require('joi') 
const schemas = { 
  makeCreate: Joi.object({
    vehicleCategoryId: Joi.array().items(Joi.number()), 
    makeName: Joi.string().regex(/^[ A-Za-z0-9_@./#&+-]*$/).lowercase().required()
                .messages({
                         'string.pattern.base': `Name is only alphanumaric`
                       }),
    makeSAPCode: Joi.string().regex(/^[a-z0-9]+(?:-[a-z0-9]+)*$/).lowercase().required()
    .messages({
             'string.pattern.base': `Code is only alphanumaric`
           }) 
  }),
  makeUpdate: Joi.object({
    vehicleCategoryId: Joi.array().items(Joi.number()), 
    makeId:  Joi.number().required(), 
    makeName: Joi.string().regex(/^[ A-Za-z0-9_@./#&+-]*$/).lowercase().required()
                .messages({
                         'string.pattern.base': `Name is only alphanumaric`
                       }),
    makeSAPCode: Joi.string().regex(/^[a-z0-9]+(?:-[a-z0-9]+)*$/).lowercase().required()
    .messages({
             'string.pattern.base': `Code is only alphanumaric`
           }) 
  }),
  makeDelete: Joi.object({
    makeId:  Joi.number().required() 
  }),
  makeGetById: Joi.object({
    makeId:  Joi.number().required() 
  }),
  
  // MODEL CURD

  modelCreate: Joi.object({
    makeId:  Joi.number().required() ,
    maxWeight:  Joi.number().required() ,
    uom: Joi.string().regex(/^[a-z0-9]+(?:-[a-z0-9]+)*$/).lowercase().required()
        .messages({
                'string.pattern.base': `UOM is only alphanumaric`
              }),
    oldMaterialNumber: Joi.string().regex(/^[a-z0-9]+(?:-[a-z0-9]+)*$/).lowercase().required()
        .messages({
                  'string.pattern.base': `UOM is only alphanumaric`
                }),                                                 
    model: Joi.string().regex(/^[ A-Za-z0-9_@./#&+-]*$/).lowercase().required()
          .messages({
             'string.pattern.base': `Code is only alphanumaric`
           }),
    materialDesc: Joi.string().regex(/^[ A-Za-z0-9_@./#&+-]*$/).lowercase().required()
          .messages({
            'string.pattern.base': `Code is only alphanumaric`
          }),
    vehicleCategoryId:  Joi.number().required(),

  }),
  modelUpdate: Joi.object({
    modelId:  Joi.number().required() ,
    makeId:  Joi.number().required() ,
    maxWeight:  Joi.number().required() ,
    uom: Joi.string().regex(/^[a-z0-9]+(?:-[a-z0-9]+)*$/).lowercase().required()
        .messages({
                'string.pattern.base': `UOM is only alphanumaric`
              }),
    oldMaterialNumber: Joi.string().regex(/^[a-z0-9]+(?:-[a-z0-9]+)*$/).lowercase().required()
        .messages({
                  'string.pattern.base': `UOM is only alphanumaric`
                }),                                                 
    model: Joi.string().regex(/^[ A-Za-z0-9_@./#&+-]*$/).lowercase().required()
          .messages({
             'string.pattern.base': `Code is only alphanumaric`
           }),
    materialDesc: Joi.string().regex(/^[ A-Za-z0-9_@./#&+-]*$/).lowercase().required()
          .messages({
            'string.pattern.base': `Code is only alphanumaric`
          }),
    vehicleCategoryId:  Joi.number().required(),

  }),
  modelGetById: Joi.object({
    modelId:  Joi.number().required() 
  }),
  modelDelete: Joi.object({
    modelId:  Joi.number().required() 
  }),
  // define all the other schemas below 
}; 
module.exports = schemas;