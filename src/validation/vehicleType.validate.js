const Joi = require('joi') 
const schemas = { 
  vehicleTypeCreate: Joi.object({
    vehicleTypeName: Joi.string().regex(/^[a-z0-9]+(?:-[a-z0-9]+)*$/).lowercase().required()
                .messages({
                         'string.pattern.base': `vehicleTypeName is only alphanumaric`
                       }),
    status: Joi.number().valid(1, 0).required(), 
  }),
  vehicleTypeUpdate: Joi.object({
    vehicleTypeId:  Joi.number().required(), 
    vehicleTypeName: Joi.string().regex(/^[a-z0-9]+(?:-[a-z0-9]+)*$/).lowercase().required()
                .messages({
                         'string.pattern.base': `vehicleTypeName is only alphanumaric`
                       }),
    status: Joi.number().valid(1, 0).required(), 
  }),
  vehicleTypeDelete: Joi.object({
    vehicleTypeId:  Joi.number().required() 
  }),
  vehicleTypeGetById: Joi.object({
    vehicleTypeId:  Joi.number().required() 
  }),
  
  // define all the other schemas below 
}; 
module.exports = schemas;