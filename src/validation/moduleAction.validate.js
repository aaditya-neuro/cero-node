const Joi = require('joi') 
const schemas = { 
  moduleActionCreate: Joi.object({
    actionName: Joi.string().regex(/^[a-z0-9]+(?:-[a-z0-9]+)*$/).lowercase().required()
                .messages({
                         'string.pattern.base': `actionName is only alphabets numebers and -`
                       }), 
    actionLevel: Joi.string()
                 .regex(/^[a-zA-Z ]*$/)
                 .required()
                 .messages({
                    'string.pattern.base': `actionLevel is accept  only alphabets and space`
                  }),
    moduleId: Joi.number().integer().required(), 
    sequence: Joi.number().required(), 
    status: Joi.number().valid(1, 0).required(), 
    isDeleted: Joi.number().valid(1, 0).optional(),
    icon: Joi.string().allow('').optional()
  }),
  moduleMappedActions: Joi.object({
      moduleId: Joi.number().integer().required(), 
   }),
  

  // define all the other schemas below 
}; 
module.exports = schemas;