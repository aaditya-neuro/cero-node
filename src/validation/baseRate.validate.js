const Joi = require('joi') 
const schemas = { 
    baseRateCreate: Joi.object({
    vehicleTypeId: Joi.number().required(),
    vehicleCategoryId: Joi.number().required(),
    basePrice: Joi.number().required(),
    relativePrice: Joi.number().required(),
    status: Joi.number().required(),
  }),
  baseRateUpdate: Joi.object({
    baseRateId:  Joi.number().required(), 
    vehicleTypeId: Joi.number().required(),
    vehicleCategoryId: Joi.number().required(),
    basePrice: Joi.number().required(),
    relativePrice: Joi.number().required(),
    status: Joi.number().required(),
  }),
  baseRateDelete: Joi.object({
    baseRateId:  Joi.number().required() 
  }),
  baseRateGetById: Joi.object({
    baseRateId:  Joi.number().required() 
  }),
  
  // define all the other schemas below 
}; 
module.exports = schemas;