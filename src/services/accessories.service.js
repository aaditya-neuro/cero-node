const conn = require("../../models");
// const Modules = conn.Modules;
const AccesoriesMaster = conn.AccesoriesMaster;
const Op = conn.Sequelize.Op;
var fs = require('fs'),
async = require('async'),
csv = require('csv');
const { takeCoverage } = require("v8");
let AccessoriesDataProvider = {
  getAccessoriesList: async() => {
      //const User = dbRepo[dbKey].User;
      return new Promise(function(resolve, reject) {
        AccesoriesMaster.findAll({ where: { status:0}})
          .then(data => {
            console.log("datatata",data);
            resolve(data);
          }).catch(err => {
            reject(err);
          });
      });
    },
    createAccessories: async(body) => {
   
      return new Promise(function(resolve, reject) {
        AccesoriesMaster.create(body)
          .then(data => {
            resolve(data);
          }).catch(err => {
            reject(err);
          });
      });
    },
    updateAccessories: async(body) => {
      return new Promise(function(resolve, reject) {
        AccesoriesMaster.update({
          accessoriesName      : body.accessoriesName,
          accessoriesPrice     : body.accessoriesPrice,
          updated_by           : 0,
          status               : body.status ? body.status : 1
        },{
          where: { id:body.accessoriesId },
        })
          .then(data => {
            resolve(data);
          }).catch(err => {
            reject(err);
          });
      });
    },
    deleteAccessories: async(accessoriesId) => {
      return new Promise(function(resolve, reject) {
        AccesoriesMaster.update({
            status               : 1
          },
            { where: { id:accessoriesId },
        })
          .then(data => {
            resolve(data);
          }).catch(err => {
            reject(err);
          });
      });
    },
    getByIdAccessories: async(accessoriesId) => {
      return new Promise(function(resolve, reject) {
        AccesoriesMaster.findOne({
          where: { id:accessoriesId },
        })
          .then(data => {
            resolve(data);
          }).catch(err => {
            reject(err);
          });
      });
    },
    bulkUploadAccessories: async(filename) => {
      return new Promise(function(resolve, reject) {
        var input = fs.createReadStream("./assets/uploads/"+filename);
        var parser = csv.parse({
          columns: true,
          relax: true
        });
        var inserter = async.cargo(function(tasks, inserterCallback) {
          //console.log("tasks",tasks)

          AccesoriesMaster.bulkCreate(tasks).then(function() {
                inserterCallback(); 
              }
            ).catch(err => {
              reject(err);
            });
          },
          1000
        );
        parser.on('readable', function () {
          while(line = parser.read()) {
            inserter.push(line);
          }
        });
        parser.on('end', function (count) {
          inserter.drain = function() {
            fs.unlinkSync("./assets/uploads/"+filename)
            resolve("");
          }
        });
        input.pipe(parser);
      });
    },

};

module.exports = AccessoriesDataProvider;
