const conn = require("../../models");
// const Modules = conn.Modules;
const vehicleScrapPriceMaster = conn.VehicleScrapPrice;
const Op = conn.Sequelize.Op;
var fs = require('fs'),
async = require('async'),
csv = require('csv');

let VehicleScrapPriceDataProvider = {
  getVehicleScrapPriceList: async() => {
      //const User = dbRepo[dbKey].User;
      return new Promise(function(resolve, reject) {
        vehicleScrapPriceMaster.findAll({ where: { status:0}})
          .then(data => {
            console.log("datatata",data);
            resolve(data);
          }).catch(err => {
            reject(err);
          });
      });
    },
    createVehicleScrapPrice: async(body) => {
   
      return new Promise(function(resolve, reject) {
        vehicleScrapPriceMaster.create(body)
          .then(data => {
            resolve(data);
          }).catch(err => {
            reject(err);
          });
      });
    },
    updateVehicleScrapPrice: async(body) => {
      return new Promise(function(resolve, reject) {
        vehicleScrapPriceMaster.update({
          vehicleCategoryId: body.vehicleCategoryId,
          vehicleTypeId: body.vehicleTypeId,
          vehicleScrapBasePrice : body.vehicleScrapBasePrice,
          status                       : body.status ? body.status : 1
        },{
          where: { id:body.vehicleScrapPriceId },
        })
          .then(data => {
            resolve(data);
          }).catch(err => {
            reject(err);
          });
      });
    },
    deleteVehicleScrapPrice: async(vehicleScrapPriceId) => {
      return new Promise(function(resolve, reject) {
        vehicleScrapPriceMaster.destroy({
          where: { id:vehicleScrapPriceId },
        })
          .then(data => {
            resolve(data);
          }).catch(err => {
            reject(err);
          });
      });
    },
    getByIdVehicleScrapPrice: async(vehicleScrapPriceId) => {
      return new Promise(function(resolve, reject) {
        vehicleScrapPriceMaster.findOne({
          where: { id:vehicleScrapPriceId },
        })
          .then(data => {
            resolve(data);
          }).catch(err => {
            reject(err);
          });
      });
    },
    bulkUploadVehicleScrapPrice: async(filename) => {
      return new Promise(function(resolve, reject) {
        var input = fs.createReadStream("./assets/uploads/"+filename);
        var parser = csv.parse({
          columns: true,
          relax: true
        });
        var inserter = async.cargo(function(tasks, inserterCallback) {
          vehicleScrapPriceMaster.bulkCreate(tasks).then(function() {
                inserterCallback(); 
              }
            ).catch(err => {
              reject(err);
            });
          },
          1000
        );
        parser.on('readable', function () {
          while(line = parser.read()) {
            inserter.push(line);
          }
        });
        parser.on('end', function (count) {
          inserter.drain = function() {
            fs.unlinkSync("./assets/uploads/"+filename)
            resolve("");
          }
        });
        input.pipe(parser);
      });
    },
};

module.exports = VehicleScrapPriceDataProvider;
