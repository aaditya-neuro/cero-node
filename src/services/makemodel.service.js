const conn = require("../../models");
// const Modules = conn.Modules;
const cero_make_master = conn.cero_make_master;
const cero_model_master = conn.cero_model_master;
const vehicleCategoryMaster = conn.VehicleCategory;


const Op = conn.Sequelize.Op;
var fs = require('fs'),
async = require('async'),
csv = require('csv');
const { takeCoverage } = require("v8");
let MakeModelDataProvider = {
    getMakeList: async() => {
      //const User = dbRepo[dbKey].User;
      return new Promise(function(resolve, reject) {
          cero_make_master.findAll({ where: { status:0}})
          .then(data => {
            console.log("datatata",data);
            resolve(data);
          }).catch(err => {
            reject(err);
          });
      });
    },
    createMake: async(body) => {
   
      return new Promise(function(resolve, reject) {
        cero_make_master.create(body)
          .then(data => {
            resolve(data);
          }).catch(err => {
            reject(err);
          });
      });
    },
    updateMake: async(body) => {
      return new Promise(function(resolve, reject) {
        cero_make_master.update({
            make           : body.make,
            make_sap_code  : body.make_sap_code,
            vehicle_category_id  : body.vehicle_category_id,
            status         : body.status ? body.status:0,

        },{
          where: { id:body.makeId },
        })
          .then(data => {
            resolve(data);
          }).catch(err => {
            reject(err);
          });
      });
    },
    deleteMake: async(makeId) => {
      return new Promise(function(resolve, reject) {
        cero_make_master.destroy({
          where: { id:makeId },
        })
          .then(data => {
            resolve(data);
          }).catch(err => {
            reject(err);
          });
      });
    },
    getByIdMake: async(makeId) => {
      return new Promise(function(resolve, reject) {
        cero_make_master.findOne({
          where: { id:makeId },
        })
          .then(data => {
            resolve(data);
          }).catch(err => {
            reject(err);
          });
      });
    },
    getMakeCode: async(makeId) => {
      return new Promise(function(resolve, reject) {
        cero_make_master.findOne({
          where: { id:makeId},
        })
          .then(data => {
            resolve(data.make_sap_code);
          }).catch(err => {
            reject(err);
          });
      });
    },

  // MODEL CURD

  getModelList: async() => {
    //const User = dbRepo[dbKey].User;
    return new Promise(function(resolve, reject) {
        cero_model_master.findAll({ where: { status:0}})
        .then(data => {
          console.log("datatata",data);
          resolve(data);
        }).catch(err => {
          reject(err);
        });
    });
  },
  createModel: async(body) => {
    return new Promise(function(resolve, reject) {
      cero_model_master.create(body)
        .then(data => {
          resolve(data);
        }).catch(err => {
          reject(err);
        });
    });
  },
  getByIdModel: async(modelId) => {
    return new Promise(function(resolve, reject) {
      cero_model_master.findOne({
        where: { id:modelId },
      })
        .then(data => {
          resolve(data);
        }).catch(err => {
          reject(err);
        });
    });
  },
  updateModel: async(body) => {
    return new Promise(function(resolve, reject) {
      cero_model_master.update({
          make_id              : body.make_id,
          max_weight           : body.max_weight,
          uom                  : body.uom,
          old_material_number  : body.old_material_number,
          model                : body.model,
          material_desc        : body.material_desc,
          vehicle_category_id  : body.vehicle_category_id,

      },{
        where: { id:body.modelId },
      })
        .then(data => {
          resolve(data);
        }).catch(err => {
          reject(err);
        });
    });
  },
  deleteModel: async(modelId) => {
    return new Promise(function(resolve, reject) {
      cero_model_master.destroy({
        where: { id:modelId },
      })
        .then(data => {
          resolve(data);
        }).catch(err => {
          reject(err);
        });
    });
  },
  getLatestRMCode: async(rmCodePrefix) => {
    return new Promise(function(resolve, reject) {
      cero_model_master.findOne({
        where: { rm_code: {[Op.like]: `${rmCodePrefix}%`} },
        order: [
          ['rm_code', 'DESC']
      ]
      })
        .then(data => {
          // console.log(data);
          if(data){
            resolve(data.rm_code);
          }else{
            resolve(`${rmCodePrefix}0000`);
          }
          
        }).catch(err => {
          reject(err);
        });
    });
  },
  getVehicleCatCodeByMake: async(vehicleCategoryId) => {
    return new Promise(function(resolve, reject) {
      vehicleCategoryMaster.findOne({
        where: { id: vehicleCategoryId },
      })
        .then(data => {
          // console.log(data);
          if(data){
            resolve(data.vehicleCategoryalfa);
          }else{
            reject(err);
          }
        }).catch(err => {
          reject(err);
        });
    });
  },

};

module.exports = MakeModelDataProvider;
