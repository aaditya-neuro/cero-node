const conn = require("../../models");
// const Modules = conn.Modules;
const { QueryTypes } = require('sequelize');

const country_master = conn.country_master;
const state_master = conn.state_master;
const city_master = conn.city_master;
const pincode_master = conn.pincode_master;

const Op = conn.Sequelize.Op;
var fs = require('fs'),
async = require('async'),
csv = require('csv');
const { takeCoverage } = require("v8");
let skippedentries = [];
let LocationDataProvider = {

  // COUNTRY SERVICES
  getCountryList: async() => {
      //const User = dbRepo[dbKey].User;
      return new Promise(function(resolve, reject) {
        country_master.findAll({ where: { status:0}})
          .then(data => {
            console.log("datatata",data);
            resolve(data);
          }).catch(err => {
            reject(err);
          });
      });
    },
    createCountry: async(body) => {
   
      return new Promise(function(resolve, reject) {
        country_master.create(body)
          .then(data => {
            resolve(data);
          }).catch(err => {
            reject(err);
          });
      });
    },
    getByIdCountry: async(countryId) => {
      return new Promise(function(resolve, reject) {
        country_master.findOne({
          where: { id:countryId },
        })
          .then(data => {
            resolve(data);
          }).catch(err => {
            reject(err);
          });
      });
    },
    updateCountry: async(body) => {
      return new Promise(function(resolve, reject) {
        country_master.update({
          name           : body.countryName
        },{
          where: { id:body.countryId },
        })
          .then(data => {
            resolve(data);
          }).catch(err => {
            reject(err);
          });
      });
    },
    deleteCountry: async(countryId) => {
      return new Promise(function(resolve, reject) {
        country_master.destroy({
          where: { id:countryId },
        })
          .then(data => {
            resolve(data);
          }).catch(err => {
            reject(err);
          });
      });
    },

    // STATE SERVICES
    getStateList: async() => {
      //const User = dbRepo[dbKey].User;
      return new Promise(function(resolve, reject) {
        state_master.findAll({ where: { status:0}})
          .then(data => {
            console.log("datatata",data);
            resolve(data);
          }).catch(err => {
            reject(err);
          });
      });
    },
    createState: async(body) => {
   
      return new Promise(function(resolve, reject) {
        state_master.create(body)
          .then(data => {
            resolve(data);
          }).catch(err => {
            reject(err);
          });
      });
    },
    getByIdState: async(stateId) => {
      return new Promise(function(resolve, reject) {
        state_master.findOne({
          where: { id:stateId },
        })
          .then(data => {
            resolve(data);
          }).catch(err => {
            reject(err);
          });
      });
    },
    updateState: async(body) => {
      return new Promise(function(resolve, reject) {
        state_master.update({
          name           : body.stateName,
          countryId      :body.countryId
        },{
          where: { id:body.stateId },
        })
          .then(data => {
            resolve(data);
          }).catch(err => {
            reject(err);
          });
      });
    },
    deleteState: async(stateId) => {
      return new Promise(function(resolve, reject) {
        state_master.destroy({
          where: { id:stateId },
        })
          .then(data => {
            resolve(data);
          }).catch(err => {
            reject(err);
          });
      });
    },

    getByCountryState: async(countryId) => {
      return new Promise(function(resolve, reject) {
        state_master.findAll({
          where: { countryId:countryId },
        })
          .then(data => {
            resolve(data);
          }).catch(err => {
            reject(err);
          });
      });
    },
        // CITY SERVICES
        getCityList: async() => {
          //const User = dbRepo[dbKey].User;
          return new Promise(function(resolve, reject) {
            city_master.findAll({ where: { status:0}})
              .then(data => {
                console.log("datatata",data);
                resolve(data);
              }).catch(err => {
                reject(err);
              });
          });
        },
        createCity: async(body) => {
       
          return new Promise(function(resolve, reject) {
            city_master.create(body)
              .then(data => {
                resolve(data);
              }).catch(err => {
                reject(err);
              });
          });
        },
        getByIdCity: async(cityId) => {
          return new Promise(function(resolve, reject) {
            city_master.findOne({
              where: { id:cityId },
            })
              .then(data => {
                resolve(data);
              }).catch(err => {
                reject(err);
              });
          });
        },
        getByStateCity: async(stateId) => {
          return new Promise(function(resolve, reject) {
            city_master.findAll({
              where: { stateId:stateId },
            })
              .then(data => {
                resolve(data);
              }).catch(err => {
                reject(err);
              });
          });
        },
        updateCity: async(body) => {
          return new Promise(function(resolve, reject) {
            city_master.update({
              name           : body.cityName,
              stateId      :body.stateId
            },{
              where: { id:body.cityId },
            })
              .then(data => {
                resolve(data);
              }).catch(err => {
                reject(err);
              });
          });
        },
        deleteCity: async(cityId) => {
          return new Promise(function(resolve, reject) {
            city_master.destroy({
              where: { id:cityId },
            })
              .then(data => {
                resolve(data);
              }).catch(err => {
                reject(err);
              });
          });
        },
        getServicableCity: async() => {
          //const User = dbRepo[dbKey].User;
          return new Promise(async function(resolve, reject) {
            let result = await conn.sequelize.query(`SELECT cm.id,cm.name FROM city_masters cm LEFT JOIN cero_plant_masters cpm ON cm.id = ANY (cpm.serviceable_city::int[])  
            WHERE cpm.serviceable_city IS NULL`,{type: QueryTypes.SELECT });
            if(result){
              resolve(result);
            }else{
              reject("err");
            }

          });
        },
                // PINCODE SERVICES
                getPincodeList: async() => {
                  //const User = dbRepo[dbKey].User;
                  return new Promise(function(resolve, reject) {
                    pincode_master.findAll({ where: { status:0}})
                      .then(data => {
                        console.log("datatata",data);
                        resolve(data);
                      }).catch(err => {
                        reject(err);
                      });
                  });
                },
                createPincode: async(body) => {
               
                  return new Promise(function(resolve, reject) {
                    pincode_master.create(body)
                      .then(data => {
                        resolve(data);
                      }).catch(err => {
                        reject(err);
                      });
                  });
                },
                getByIdPincode: async(pincodeId) => {
                  return new Promise(function(resolve, reject) {
                    pincode_master.findOne({
                      where: { id:pincodeId },
                    })
                      .then(data => {
                        resolve(data);
                      }).catch(err => {
                        reject(err);
                      });
                  });
                },
                getByCityPincode: async(cityId) => {
                  return new Promise(function(resolve, reject) {
                    pincode_master.findAll({
                      where: { cityId:cityId },
                    })
                      .then(data => {
                        resolve(data);
                      }).catch(err => {
                        reject(err);
                      });
                  });
                },
                updatePincode: async(body) => {
                  return new Promise(function(resolve, reject) {
                    pincode_master.update({
                      name           : body.pincodeName,
                      cityId      :body.cityId
                    },{
                      where: { id:body.pincodeId },
                    })
                      .then(data => {
                        resolve(data);
                      }).catch(err => {
                        reject(err);
                      });
                  });
                },
                deletePincode: async(pincodeId) => {
                  return new Promise(function(resolve, reject) {
                    pincode_master.destroy({
                      where: { id:pincodeId },
                    })
                      .then(data => {
                        resolve(data);
                      }).catch(err => {
                        reject(err);
                      });
                  });
                },


                bulkUploadPincode: async(filename) => {
                  return new Promise(function(resolve, reject) {
                    var input = fs.createReadStream("./assets/uploads/"+filename);
                    var parser = csv.parse({
                      columns: true,
                      relax: true
                    });
                    
                    var inserter = async.cargo(async function(tasks, inserterCallback) {
                      console.log("tasks",tasks)
            
            
                      const forLoop = await fileUploadTaskLoop(tasks,reject); 
                      if(forLoop){
                        inserterCallback(); 
                      }else{

                      }
                      
                      // city_master.bulkCreate(tasks).then(function() {
                      //       
                      //     }
                      //   ).catch(err => {
                      //     reject(err);
                      //   });
                      },
                      1000
                    );
                    parser.on('readable', function () {
                      while(line = parser.read()) {
                        inserter.push(line);
                      }
                    });
                    parser.on('end', function (count) {
                      inserter.drain = function() {
                        fs.unlinkSync("./assets/uploads/"+filename)
                        resolve(skippedentries);
                      }
                    });
                    input.pipe(parser);
                  });
                },

};


function fileUploadTaskLoop(tasks,reject){
  // skippedentries =[];
  for(let i= 0 ; i< tasks.length;i++){
    // tasks[i].serviceable_city = tasks[i].serviceable_city.split(",");
    country_master.findOne({
      where: { name:tasks[i].country},
      raw:true
    })
      .then(con_data => {
        
        if(con_data !=  null){
          state_master.findOne({
            where: { name:tasks[i].state},
            raw:true
          })
            .then(stt_data => {
              
              if(stt_data !=  null){
                city_master.findOne({
                  where: { name:tasks[i].city},
                  raw:true
                })
                  .then(city_data => {
                    let city_id ;
                    if(city_data ==  null){
                      
                      city_master.create({ name:tasks[i].city,stateId:stt_data.id,active:0})
                      .then(newCityData => {
                        city_id = newCityData.id
                      }).catch(err => {
                        reject(err);
                      });
                    }else{
                      city_id = city_data.id
                    }

                    let row = { name:tasks[i].pincode,cityId:city_id,active:0}
                        pincode_master.create(row).then(function() {
                          
                        }
                      ).catch(err => {
                        reject(err);
                      });
                    
                  }).catch(err => {
                    reject(err);
                  });
              }else{
                skippedentries.push(i);
              }

            }).catch(err => {
              reject(err);
            });
        }else{
          skippedentries.push(i);
        }

      }).catch(err => {
        reject(err);
      });
  }
}

module.exports = LocationDataProvider;
