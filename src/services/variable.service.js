const conn = require("../../models");
// const Modules = conn.Modules;
const VariableMaster = conn.VariableMaster;
const Op = conn.Sequelize.Op;
var fs = require('fs'),
async = require('async'),
csv = require('csv');
const { takeCoverage } = require("v8");
let VariableDataProvider = {
  getVariableList: async() => {
      //const User = dbRepo[dbKey].User;
      return new Promise(function(resolve, reject) {
        VariableMaster.findAll({ where: { status:0}})
          .then(data => {
            console.log("datatata",data);
            resolve(data);
          }).catch(err => {
            reject(err);
          });
      });
    },
    createVariable: async(body) => {
   
      return new Promise(function(resolve, reject) {
        VariableMaster.create(body)
          .then(data => {
            resolve(data);
          }).catch(err => {
            reject(err);
          });
      });
    },
    updateVariable: async(body) => {
      return new Promise(function(resolve, reject) {
        VariableMaster.update({
            variableType       : body.variableType,
            variableValue      : body.variableValue,
            price              : body.price,
          updated_by           : 0,
          status               : body.status ? body.status : 1
        },{
          where: { id:body.variableId },
        })
          .then(data => {
            resolve(data);
          }).catch(err => {
            reject(err);
          });
      });
    },
    deleteVariable: async(variableId) => {
      return new Promise(function(resolve, reject) {
        VariableMaster.update({
            status               : 1
          },
            { where: { id:variableId },
        })
          .then(data => {
            resolve(data);
          }).catch(err => {
            reject(err);
          });
      });
    },
    getByIdVariable: async(variableId) => {
      return new Promise(function(resolve, reject) {
        VariableMaster.findOne({
          where: { id:variableId },
        })
          .then(data => {
            resolve(data);
          }).catch(err => {
            reject(err);
          });
      });
    },
    bulkUploadVariable: async(filename) => {
      return new Promise(function(resolve, reject) {
        var input = fs.createReadStream("./assets/uploads/"+filename);
        var parser = csv.parse({
          columns: true,
          relax: true
        });
        var inserter = async.cargo(function(tasks, inserterCallback) {
          //console.log("tasks",tasks)

          VariableMaster.bulkCreate(tasks).then(function() {
                inserterCallback(); 
              }
            ).catch(err => {
              reject(err);
            });
          },
          1000
        );
        parser.on('readable', function () {
          while(line = parser.read()) {
            inserter.push(line);
          }
        });
        parser.on('end', function (count) {
          inserter.drain = function() {
            fs.unlinkSync("./assets/uploads/"+filename)
            resolve("");
          }
        });
        input.pipe(parser);
      });
    },

};

module.exports = VariableDataProvider;
