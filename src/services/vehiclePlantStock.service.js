const conn = require("../../models");
// const Modules = conn.Modules;
const VehiclePlantStockMaster = conn.VehiclePlantStockMaster;
const Op = conn.Sequelize.Op;
var fs = require('fs'),
async = require('async'),
csv = require('csv');
const { takeCoverage } = require("v8");
let VehiclePlantStockDataProvider = {
  getVehiclePlantStockList: async() => {
      //const User = dbRepo[dbKey].User;
      return new Promise(function(resolve, reject) {
        VehiclePlantStockMaster.findAll({ where: { status:0}})
          .then(data => {
            console.log("datatata",data);
            resolve(data);
          }).catch(err => {
            reject(err);
          });
      });
    },
    createVehiclePlantStock: async(body) => {
   
      return new Promise(function(resolve, reject) {
        VehiclePlantStockMaster.create(body)
          .then(data => {
            resolve(data);
          }).catch(err => {
            reject(err);
          });
      });
    },
    updateVehiclePlantStock: async(body) => {
      return new Promise(function(resolve, reject) {
        VehiclePlantStockMaster.update({
          plantId                 : body.plantId,
        rmCode                    : body.rmCode,
        stockLevel                : body.stockLevel,
        demand                    : body.demand,
          updated_by              : 0,
          status                  : body.status ? body.status : 1
        },{
          where: { id:body.vehiclePlantStockId },
        })
          .then(data => {
            resolve(data);
          }).catch(err => {
            reject(err);
          });
      });
    },
    deleteVehiclePlantStock: async(vehiclePlantStockId) => {
      return new Promise(function(resolve, reject) {
        VehiclePlantStockMaster.update({
            status               : 1
          },
            { where: { id:vehiclePlantStockId },
        })
          .then(data => {
            resolve(data);
          }).catch(err => {
            reject(err);
          });
      });
    },
    getByIdVehiclePlantStock: async(vehiclePlantStockId) => {
      return new Promise(function(resolve, reject) {
        VehiclePlantStockMaster.findOne({
          where: { id:vehiclePlantStockId },
        })
          .then(data => {
            resolve(data);
          }).catch(err => {
            reject(err);
          });
      });
    },
    bulkUploadVehiclePlantStock: async(filename) => {
      return new Promise(function(resolve, reject) {
        var input = fs.createReadStream("./assets/uploads/"+filename);
        var parser = csv.parse({
          columns: true,
          relax: true
        });
        var inserter = async.cargo(function(tasks, inserterCallback) {
          //console.log("tasks",tasks)

          VehiclePlantStockMaster.bulkCreate(tasks).then(function() {
                inserterCallback(); 
              }
            ).catch(err => {
              reject(err);
            });
          },
          1000
        );
        parser.on('readable', function () {
          while(line = parser.read()) {
            inserter.push(line);
          }
        });
        parser.on('end', function (count) {
          inserter.drain = function() {
            fs.unlinkSync("./assets/uploads/"+filename)
            resolve("");
          }
        });
        input.pipe(parser);
      });
    },

};

module.exports = VehiclePlantStockDataProvider;
