const conn = require("../../models");
const Modules = conn.Modules;
const modulesAction = conn.ModulesActions;
const Op = conn.Sequelize.Op;

let ModuleDataProvider = {

  getModuleActions: async() => {
    //const User = dbRepo[dbKey].User;
    return new Promise(function(resolve, reject) {
        modulesAction.findAll()
        .then(data => {
          console.log("datatata",data);
          resolve(data);
        }).catch(err => {
          reject(err);
        });
    });
  },

  createModuleAction: async(body) => {
   
    return new Promise(function(resolve, reject) {
        modulesAction.create(body)
        .then(data => {
          resolve(data);
        }).catch(err => {
          reject(err);
        });
    });
  },

  moduleMappedActions: async(id) => {
    let moduleId=  id;
    return new Promise(function(resolve, reject) {
      Modules.findOne({
        where: { id: moduleId, status: 1 },
        include: [
          {
              model:modulesAction,
              attributes : ['id','actionName','actionLevel'],
              required: false,                  

             }
        ]
      })
        .then(data => {
          resolve(data);
        }).catch(err => {
          reject(err);
        });
    });
  },

//   getUser: async(userId, dbKey) => {
//     const User = dbRepo[dbKey].User;
//     return new Promise(function(resolve, reject) {
//       User.findOne({ where: { id: userId }, attributes: { exclude: ['password'] } })
//         .then(data => {
//           resolve(data);
//         }).catch(err => {
//           reject(err);
//         });
//     });
//   },

 

//   updateUser: async() => {
//     return null;
//   },

//   deleteUser: async(userId, dbKey) => {
//     const User = dbRepo[dbKey].User;
//     return new Promise(function(resolve, reject) {
//       User.destroy({ where: { id: userId } })
//         .then(data => {
//           resolve(data);
//         }).catch(err => {
//           reject(err);
//         });
//     });
//   }

};

module.exports = ModuleDataProvider;
