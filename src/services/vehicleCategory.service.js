const conn = require("../../models");
// const Modules = conn.Modules;
const vehicleCategoryMaster = conn.VehicleCategory;
const Op = conn.Sequelize.Op;
var fs = require('fs'),
async = require('async'),
csv = require('csv');

let VehicleCategoryDataProvider = {
  getVehicleCategoryList: async() => {
      //const User = dbRepo[dbKey].User;
      return new Promise(function(resolve, reject) {
        vehicleCategoryMaster.findAll({ where: { status:0}})
          .then(data => {
            console.log("datatata",data);
            resolve(data);
          }).catch(err => {
            reject(err);
          });
      });
    },
    createVehicleCategory: async(body) => {
   
      return new Promise(function(resolve, reject) {
        vehicleCategoryMaster.create(body)
          .then(data => {
            resolve(data);
          }).catch(err => {
            reject(err);
          });
      });
    },
    updateVehicleCategory: async(body) => {
      return new Promise(function(resolve, reject) {
        vehicleCategoryMaster.update({
          vehicleCategoryName          : body.vehicleCategoryName,
          vehicleCategoryCode          : body.vehicleCategoryCode,
          vehicleCategoryalfa          : body.vehicleCategoryalfa,
          status                       : body.status ? body.status : 1
        },{
          where: { id:body.vehicleCategoryId },
        })
          .then(data => {
            resolve(data);
          }).catch(err => {
            reject(err);
          });
      });
    },
    deleteVehicleCategory: async(vehicleCategoryId) => {
      return new Promise(function(resolve, reject) {
        vehicleCategoryMaster.destroy({
          where: { id:vehicleCategoryId },
        })
          .then(data => {
            resolve(data);
          }).catch(err => {
            reject(err);
          });
      });
    },
    getByIdVehicleCategory: async(vehicleCategoryId) => {
      return new Promise(function(resolve, reject) {
        vehicleCategoryMaster.findOne({
          where: { id:vehicleCategoryId },
        })
          .then(data => {
            resolve(data);
          }).catch(err => {
            reject(err);
          });
      });
    },
    bulkUploadVehicleCategory: async(filename) => {
      return new Promise(function(resolve, reject) {
        var input = fs.createReadStream("./assets/uploads/"+filename);
        var parser = csv.parse({
          columns: true,
          relax: true
        });
        var inserter = async.cargo(function(tasks, inserterCallback) {
          vehicleCategoryMaster.bulkCreate(tasks).then(function() {
                inserterCallback(); 
              }
            ).catch(err => {
              reject(err);
            });
          },
          1000
        );
        parser.on('readable', function () {
          while(line = parser.read()) {
            inserter.push(line);
          }
        });
        parser.on('end', function (count) {
          inserter.drain = function() {
            fs.unlinkSync("./assets/uploads/"+filename)
            resolve("");
          }
        });
        input.pipe(parser);
      });
    },
};

module.exports = VehicleCategoryDataProvider;
