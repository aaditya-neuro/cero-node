const conn = require("../../models");
// const Modules = conn.Modules;
const vehicleTypeMaster = conn.VehicleType;
const Op = conn.Sequelize.Op;
var fs = require('fs'),
async = require('async'),
csv = require('csv');

let VehicleTypeDataProvider = {
  getVehicleTypeList: async() => {
      //const User = dbRepo[dbKey].User;
      return new Promise(function(resolve, reject) {
        vehicleTypeMaster.findAll({ where: { status:0}})
          .then(data => {
            console.log("datatata",data);
            resolve(data);
          }).catch(err => {
            reject(err);
          });
      });
    },
    createVehicleType: async(body) => {
   
      return new Promise(function(resolve, reject) {
        vehicleTypeMaster.create(body)
          .then(data => {
            resolve(data);
          }).catch(err => {
            reject(err);
          });
      });
    },
    updateVehicleType: async(body) => {
      return new Promise(function(resolve, reject) {
        vehicleTypeMaster.update({
          vehicleTypeName          : body.vehicleTypeName,
          status                       : body.status ? body.status : 1
        },{
          where: { id:body.vehicleTypeId },
        })
          .then(data => {
            resolve(data);
          }).catch(err => {
            reject(err);
          });
      });
    },
    deleteVehicleType: async(vehicleTypeId) => {
      return new Promise(function(resolve, reject) {
        vehicleTypeMaster.destroy({
          where: { id:vehicleTypeId },
        })
          .then(data => {
            resolve(data);
          }).catch(err => {
            reject(err);
          });
      });
    },
    getByIdVehicleType: async(vehicleTypeId) => {
      return new Promise(function(resolve, reject) {
        vehicleTypeMaster.findOne({
          where: { id:vehicleTypeId },
        })
          .then(data => {
            resolve(data);
          }).catch(err => {
            reject(err);
          });
      });
    },
    bulkUploadVehicleType: async(filename) => {
      return new Promise(function(resolve, reject) {
        var input = fs.createReadStream("./assets/uploads/"+filename);
        var parser = csv.parse({
          columns: true,
          relax: true
        });
        var inserter = async.cargo(function(tasks, inserterCallback) {
          vehicleTypeMaster.bulkCreate(tasks).then(function() {
                inserterCallback(); 
              }
            ).catch(err => {
              reject(err);
            });
          },
          1000
        );
        parser.on('readable', function () {
          while(line = parser.read()) {
            inserter.push(line);
          }
        });
        parser.on('end', function (count) {
          inserter.drain = function() {
            fs.unlinkSync("./assets/uploads/"+filename)
            resolve("");
          }
        });
        input.pipe(parser);
      });
    },
};

module.exports = VehicleTypeDataProvider;
