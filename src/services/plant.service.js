const conn = require("../../models");
// const Modules = conn.Modules;
const cero_plant_master = conn.cero_plant_master;
const Op = conn.Sequelize.Op;
var fs = require('fs'),
async = require('async'),
csv = require('csv');
const { takeCoverage } = require("v8");
let PlantDataProvider = {
  getPlantList: async() => {
      //const User = dbRepo[dbKey].User;
      return new Promise(function(resolve, reject) {
          cero_plant_master.findAll({ where: { status:0}})
          .then(data => {
            console.log("datatata",data);
            resolve(data);
          }).catch(err => {
            reject(err);
          });
      });
    },
    createPlant: async(body) => {
   
      return new Promise(function(resolve, reject) {
        cero_plant_master.create(body)
          .then(data => {
            resolve(data);
          }).catch(err => {
            reject(err);
          });
      });
    },
    updatePlant: async(body) => {
      return new Promise(function(resolve, reject) {
        cero_plant_master.update({
          plant_name           : body.plantName,
          state               : body.state,
          city                : body.city,
          address             : body.address,
          serviceable_city     : body.serviceableCity,
          Created_by           : body.CreatedBy,
          updated_by           : body.updatedBy,
          status              : body.status ? body.status : 1
        },{
          where: { id:body.plantId },
        })
          .then(data => {
            resolve(data);
          }).catch(err => {
            reject(err);
          });
      });
    },
    deletePlant: async(plantId) => {
      return new Promise(function(resolve, reject) {
        cero_plant_master.destroy({
          where: { id:plantId },
        })
          .then(data => {
            resolve(data);
          }).catch(err => {
            reject(err);
          });
      });
    },
    getByIdPlant: async(plantId) => {
      return new Promise(function(resolve, reject) {
        cero_plant_master.findOne({
          where: { id:plantId },
        })
          .then(data => {
            resolve(data);
          }).catch(err => {
            reject(err);
          });
      });
    },
    bulkUploadPlant: async(filename) => {
      return new Promise(function(resolve, reject) {
        var input = fs.createReadStream("./assets/uploads/"+filename);
        var parser = csv.parse({
          columns: true,
          relax: true
        });
        var inserter = async.cargo(function(tasks, inserterCallback) {
          //console.log("tasks",tasks)


          for(let i= 0 ; i< tasks.length;i++){
            tasks[i].serviceable_city = tasks[i].serviceable_city.split(",");
          }

          cero_plant_master.bulkCreate(tasks).then(function() {
                inserterCallback(); 
              }
            ).catch(err => {
              reject(err);
            });
          },
          1000
        );
        parser.on('readable', function () {
          while(line = parser.read()) {
            inserter.push(line);
          }
        });
        parser.on('end', function (count) {
          inserter.drain = function() {
            fs.unlinkSync("./assets/uploads/"+filename)
            resolve("");
          }
        });
        input.pipe(parser);
      });
    },

};

module.exports = PlantDataProvider;
