const conn = require("../../models");
// const Modules = conn.Modules;
const BaseRate = conn.BaseRate;
const Op = conn.Sequelize.Op;
var fs = require('fs'),
async = require('async'),
csv = require('csv');
const { takeCoverage } = require("v8");
let BaseRateDataProvider = {
  getBaseRateList: async() => {
      //const User = dbRepo[dbKey].User;
      return new Promise(function(resolve, reject) {
        BaseRate.findAll({ where: { status:0}})
          .then(data => {
            console.log("datatata",data);
            resolve(data);
          }).catch(err => {
            reject(err);
          });
      });
    },
    createBaseRate: async(body) => {
   
      return new Promise(function(resolve, reject) {
        BaseRate.create(body)
          .then(data => {
            resolve(data);
          }).catch(err => {
            reject(err);
          });
      });
    },
    updateBaseRate: async(body) => {
      return new Promise(function(resolve, reject) {
        BaseRate.update({
            vehicleTypeId       : body.vehicleTypeId,
            vehicleCategoryId      : body.vehicleCategoryId,
            basePrice      : body.basePrice,
            relativePrice      : body.relativePrice,
          updated_by           : 0,
          status               : body.status ? body.status : 1
        },{
          where: { id:body.baseRateId },
        })
          .then(data => {
            resolve(data);
          }).catch(err => {
            reject(err);
          });
      });
    },
    deleteBaseRate: async(baseRateId) => {
      return new Promise(function(resolve, reject) {
        BaseRate.update({
            status               : 1
          },
            { where: { id:baseRateId },
        })
          .then(data => {
            resolve(data);
          }).catch(err => {
            reject(err);
          });
      });
    },
    getByIdBaseRate: async(baseRateId) => {
      return new Promise(function(resolve, reject) {
        BaseRate.findOne({
          where: { id:baseRateId },
        })
          .then(data => {
            resolve(data);
          }).catch(err => {
            reject(err);
          });
      });
    },
    bulkUploadBaseRate: async(filename) => {
      return new Promise(function(resolve, reject) {
        var input = fs.createReadStream("./assets/uploads/"+filename);
        var parser = csv.parse({
          columns: true,
          relax: true
        });
        var inserter = async.cargo(function(tasks, inserterCallback) {
          //console.log("tasks",tasks)

          BaseRate.bulkCreate(tasks).then(function() {
                inserterCallback(); 
              }
            ).catch(err => {
              reject(err);
            });
          },
          1000
        );
        parser.on('readable', function () {
          while(line = parser.read()) {
            inserter.push(line);
          }
        });
        parser.on('end', function (count) {
          inserter.drain = function() {
            fs.unlinkSync("./assets/uploads/"+filename)
            resolve("");
          }
        });
        input.pipe(parser);
      });
    },

};

module.exports = BaseRateDataProvider;
