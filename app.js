const createError = require('http-errors');
const express = require('express');
var debug = require('debug')('app.multi-tenant-node-app.api:server');
var http = require('http');
const logger = require('morgan');
const bodyParser = require('body-parser');
const moduleRouter = require('./routes/modules.route');
const moduleActionRouter = require('./routes/moduleAction.route');
const plantRouter = require('./routes/plant.route');
const locationRouter = require('./routes/location.route');
const makeModelRouter = require('./routes/makemodel.route');
const accessoriesRouter = require('./routes/accessories.route');
const baseRateRouter = require('./routes/baseRate.route');
const variableRouter = require('./routes/variable.route');
const vehiclePlantStockRouter = require('./routes/vehiclePlantStock.route');

const vehicleCategoryRouter = require('./routes/vehicleCategory.route');
const vehicleTypeRouter = require('./routes/vehicleType.route');
const vehicleScrapPriceRouter = require('./routes/vehicleScrapPrice.route');


const app = express();
app.use(logger('dev'));
app.use(express.json());
app.use(express.urlencoded({ extended: false }));
app.use(bodyParser.json());
app.use(bodyParser.urlencoded({ extended: true }));

// allow-cors
app.use(function (req, res, next) {
  res.header("Access-Control-Allow-Origin", "*");
  res.header("Access-Control-Allow-Headers", "Origin, X-Requested-With, Content-Type, Accept");
  res.header('Access-Control-Allow-Methods', 'GET, PUT, POST, DELETE, OPTIONS');
  if ('OPTIONS' === req.method) {
    //respond with 200
    res.send(200);
  }
  else {
    //move on
    next();
  }
});
app.use('/api/v1/modules',  moduleRouter);
app.use('/api/v1/moduleActions',  moduleActionRouter);
app.use('/api/v1/plant',  plantRouter);
app.use('/api/v1/location',  locationRouter);
app.use('/api/v1/makemodel',  makeModelRouter);
app.use('/api/v1/accessories',  accessoriesRouter);
app.use('/api/v1/baseRate',  baseRateRouter);
app.use('/api/v1/variable',  variableRouter);
app.use('/api/v1/vehiclePlantStock',  vehiclePlantStockRouter);
// app.use('/api/v1/location',  locationRouter);

app.use('/api/v1/vehicleCategory',  vehicleCategoryRouter);
app.use('/api/v1/vehicleType',  vehicleTypeRouter);
app.use('/api/v1/vehicleScrapPrice',  vehicleScrapPriceRouter);
// catch 404 and forward to error handler
app.use(function (req, res, next) {
  next(createError(404));
});

// error handler
app.use(function (err, req, res, next) {
  // render the error page
  res.status(err.status || 500);
  res.json({
    status: 'error',
    data: err.message,
    message: 'Something went wrong!!! Please try again later.'
  });
});



/**
 * Get port from environment and store in Express.
 */

var port = normalizePort(process.env.PORT || '5510');
console.log("Running port ",port)
app.set('port', port);

/**
 * Create HTTP server.
 */

var server = http.createServer(app);

/**
 * Listen on provided port, on all network interfaces.
 */

server.listen(port);
server.on('error', onError);
server.on('listening', onListening);

/**
 * Normalize a port into a number, string, or false.
 */

function normalizePort(val) {
  var port = parseInt(val, 10);

  if (isNaN(port)) {
    // named pipe
    return val;
  }

  if (port >= 0) {
    // port number
    return port;
  }

  return false;
}

/**
 * Event listener for HTTP server "error" event.
 */

function onError(error) {
  if (error.syscall !== 'listen') {
    throw error;
  }

  var bind = typeof port === 'string'
    ? 'Pipe ' + port
    : 'Port ' + port;

  // handle specific listen errors with friendly messages
  switch (error.code) {
    case 'EACCES':
      console.error(bind + ' requires elevated privileges');
      process.exit(1);
      break;
    case 'EADDRINUSE':
      console.error(bind + ' is already in use');
      process.exit(1);
      break;
    default:
      throw error;
  }
}

/**
 * Event listener for HTTP server "listening" event.
 */

function onListening() {
  var addr = server.address();
  var bind = typeof addr === 'string'
    ? 'pipe ' + addr
    : 'port ' + addr.port;
  debug('Listening on ' + bind);
}

module.exports = app;
