const express = require('express');
const router = express.Router();
const moduleController = require('../src/controller/modules.controller');
const uservalidate = require('../middleware/validate.middelware'); 
const schemas = require('../src/validation/module.validate'); 
router.get('/', function (request, response, next) {
    moduleController.getModules(request, response, next);
});
router.post('/create',uservalidate(schemas.moduleCreate,''), function (request, response, next) {
    moduleController.createModule(request, response, next);
});
router.post('/moduleMappedActions',uservalidate(schemas.moduleMappedActions,''), function (request, response, next) {
    moduleController.moduleMappedActions(request, response, next);
});

router.get('/allModuleMappedActions', function (request, response, next) {
    moduleController.allModuleMappedActions(request, response, next);
});

// router.get('/:userId', function (request, response, next) {
//   userController.getUser(request, response, next);
// });



// router.put('/:userId', function (request, response, next) {
//   userController.updateUser(request, response, next);
// });

// router.delete('/:userId', function (request, response, next) {
//   userController.deleteUser(request, response, next);
// });

module.exports = router;
