const express = require('express');
const router = express.Router();
const multer = require('multer')
const path = require('path')
const vehichleCategoryController = require('../src/controller/vehicleCategory.controller');
const uservalidate = require('../middleware/validate.middelware'); 
const schemas = require('../src/validation/vehicleCategory.validate'); 

//! Use of Multer
var storage = multer.diskStorage({
    destination: (req, file, callBack) => {
        callBack(null, './assets/uploads/')    
    },
    filename: (req, file, callBack) => {
        callBack(null, file.fieldname + '-' + Date.now() + path.extname(file.originalname))
    }
  })
  
  var upload = multer({
    storage: storage,
    fileFilter: (req, file, cb) => {
        if (file.mimetype == "text/csv") {
          cb(null, true);
        } else {
          cb(null, false);
          return cb(new Error('Only .csv format allowed!'));
        }
      }
  });

router.get('/', function (request, response, next) {
    vehichleCategoryController.getVehicleCategoryList(request, response, next);
});

router.post("/create",uservalidate(schemas.vehicleCategoryCreate,''),function (request,response,next){
    vehichleCategoryController.createVehicleCategory(request,response,next)
})

router.post("/getById",uservalidate(schemas.vehicleCategoryGetById,''),function (request,response,next){
    vehichleCategoryController.getByIdVehicleCategory(request,response,next)
})

router.post("/update",uservalidate(schemas.vehicleCategoryUpdate,''),function (request,response,next){
    vehichleCategoryController.updateVehicleCategory(request,response,next)
})

router.post("/delete",uservalidate(schemas.vehicleCategoryDelete,''),function (request,response,next){
    vehichleCategoryController.deleteVehicleCategory(request,response,next)
})

router.post('/uploadfile', upload.single("uploadfile"),function (request,response,next){
    console.log('CSV file data has been uploaded in mysql database ');
    vehichleCategoryController.bulkUploadVehicleCategory(request,response,next)
})


module.exports = router;
