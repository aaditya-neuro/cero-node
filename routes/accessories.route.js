const express = require('express');
const router = express.Router();
const multer = require('multer')
const path = require('path')
const accessoriesController = require('../src/controller/accessories.controller');
const uservalidate = require('../middleware/validate.middelware'); 
const schemas = require('../src/validation/accessories.validate'); 

//! Use of Multer
var storage = multer.diskStorage({
    destination: (req, file, callBack) => {
        callBack(null, './assets/uploads/')    
    },
    filename: (req, file, callBack) => {
        callBack(null, file.fieldname + '-' + Date.now() + path.extname(file.originalname))
    }
  })
  
  var upload = multer({
    storage: storage,
    fileFilter: (req, file, cb) => {
        if (file.mimetype == "text/csv") {
          cb(null, true);
        } else {
          cb(null, false);
          return cb(new Error('Only .csv format allowed!'));
        }
      }
  });

router.get('/', function (request, response, next) {
    accessoriesController.getAccessoriesList(request, response, next);
});

router.post("/create",uservalidate(schemas.accessoriesCreate,''),function (request,response,next){
    accessoriesController.createAccessories(request,response,next)
})

router.post("/getById",uservalidate(schemas.accessoriesGetById,''),function (request,response,next){
    accessoriesController.getByIdAccessories(request,response,next)
})

router.post("/update",uservalidate(schemas.accessoriesUpdate,''),function (request,response,next){
    accessoriesController.updateAccessories(request,response,next)
})

router.post("/delete",uservalidate(schemas.accessoriesDelete,''),function (request,response,next){
    accessoriesController.deleteAccessories(request,response,next)
})

router.post('/uploadfile', upload.single("uploadfile"),function (request,response,next){
    console.log('CSV file data has been uploaded in mysql database ');
    accessoriesController.bulkUploadAccessories(request,response,next)
})


module.exports = router;
