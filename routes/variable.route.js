const express = require('express');
const router = express.Router();
const multer = require('multer')
const path = require('path')
const variableController = require('../src/controller/variable.controller');
const uservalidate = require('../middleware/validate.middelware'); 
const schemas = require('../src/validation/variable.validate'); 

//! Use of Multer
var storage = multer.diskStorage({
    destination: (req, file, callBack) => {
        callBack(null, './assets/uploads/')    
    },
    filename: (req, file, callBack) => {
        callBack(null, file.fieldname + '-' + Date.now() + path.extname(file.originalname))
    }
  })
  
  var upload = multer({
    storage: storage,
    fileFilter: (req, file, cb) => {
        if (file.mimetype == "text/csv") {
          cb(null, true);
        } else {
          cb(null, false);
          return cb(new Error('Only .csv format allowed!'));
        }
      }
  });

router.get('/', function (request, response, next) {
    variableController.getVariableList(request, response, next);
});

router.post("/create",uservalidate(schemas.variableCreate,''),function (request,response,next){
    variableController.createVariable(request,response,next)
})

router.post("/getById",uservalidate(schemas.variableGetById,''),function (request,response,next){
    variableController.getByIdVariable(request,response,next)
})

router.post("/update",uservalidate(schemas.variableUpdate,''),function (request,response,next){
    variableController.updateVariable(request,response,next)
})

router.post("/delete",uservalidate(schemas.variableDelete,''),function (request,response,next){
    variableController.deleteVariable(request,response,next)
})

router.post('/uploadfile', upload.single("uploadfile"),function (request,response,next){
    console.log('CSV file data has been uploaded in mysql database ');
    variableController.bulkUploadVariable(request,response,next)
})


module.exports = router;
