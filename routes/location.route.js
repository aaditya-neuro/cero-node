const express = require('express');
const router = express.Router();
const multer = require('multer')
const path = require('path')
const locationController = require('../src/controller/location.controller');
const uservalidate = require('../middleware/validate.middelware'); 
const schemas = require('../src/validation/location.validate'); 

//! Use of Multer
var storage = multer.diskStorage({
    destination: (req, file, callBack) => {
        callBack(null, './assets/uploads/')    
    },
    filename: (req, file, callBack) => {
        callBack(null, file.fieldname + '-' + Date.now() + path.extname(file.originalname))
    }
  })
  
  var upload = multer({
    storage: storage,
    fileFilter: (req, file, cb) => {
        if (file.mimetype == "text/csv") {
          cb(null, true);
        } else {
          cb(null, false);
          return cb(new Error('Only .csv format allowed!'));
        }
      }
  });

//COUNTRY ROUTES
router.get('/country', function (request, response, next) {
    locationController.getCountryList(request, response, next);
});

router.post("/country/create",uservalidate(schemas.countryCreate,''),function (request,response,next){
    locationController.createCountry(request,response,next)
})

router.post("/country/getById",uservalidate(schemas.countryGetById,''),function (request,response,next){
    locationController.getByIdCountry(request,response,next)
})

router.post("/country/update",uservalidate(schemas.countryUpdate,''),function (request,response,next){
    locationController.updateCountry(request,response,next)
})

router.post("/country/delete",uservalidate(schemas.countryDelete,''),function (request,response,next){
    locationController.deleteCountry(request,response,next)
})

//STATE ROUTES
router.get('/state', function (request, response, next) {
    locationController.getStateList(request, response, next);
});

router.post("/state/create",uservalidate(schemas.stateCreate,''),function (request,response,next){
    locationController.createState(request,response,next)
})

router.post("/state/getById",uservalidate(schemas.stateGetById,''),function (request,response,next){
    locationController.getByIdState(request,response,next)
})

router.post("/state/update",uservalidate(schemas.stateUpdate,''),function (request,response,next){
    locationController.updateState(request,response,next)
})

router.post("/state/delete",uservalidate(schemas.stateDelete,''),function (request,response,next){
    locationController.deleteState(request,response,next)
})

router.post("/state/getByCountry",uservalidate(schemas.stateGetByCountry,''),function (request,response,next){
    locationController.getByCountryState(request,response,next)
})


//CITY ROUTES
router.get('/city', function (request, response, next) {
    locationController.getCityList(request, response, next);
});

router.post("/city/create",uservalidate(schemas.cityCreate,''),function (request,response,next){
    locationController.createCity(request,response,next)
})

router.post("/city/getById",uservalidate(schemas.cityGetById,''),function (request,response,next){
    locationController.getByIdCity(request,response,next)
})

router.post("/city/update",uservalidate(schemas.cityUpdate,''),function (request,response,next){
    locationController.updateCity(request,response,next)
})

router.post("/city/delete",uservalidate(schemas.cityDelete,''),function (request,response,next){
    locationController.deleteCity(request,response,next)
})

router.post("/city/getByState",uservalidate(schemas.cityGetByState,''),function (request,response,next){
    locationController.getByStateCity(request,response,next)
})

router.get("/city/getServicableCity",function (request,response,next){
    locationController.getServicableCity(request,response,next)
})

//PINCODE ROUTES
router.get('/pincode', function (request, response, next) {
    locationController.getPincodeList(request, response, next);
});

router.post("/pincode/create",uservalidate(schemas.pincodeCreate,''),function (request,response,next){
    locationController.createPincode(request,response,next)
})

router.post("/pincode/getById",uservalidate(schemas.pincodeGetById,''),function (request,response,next){
    locationController.getByIdPincode(request,response,next)
})

router.post("/pincode/update",uservalidate(schemas.pincodeUpdate,''),function (request,response,next){
    locationController.updatePincode(request,response,next)
})

router.post("/pincode/delete",uservalidate(schemas.pincodeDelete,''),function (request,response,next){
    locationController.deletePincode(request,response,next)
})

router.post("/pincode/getByCity",uservalidate(schemas.pincodeGetByCity,''),function (request,response,next){
    locationController.getByCityPincode(request,response,next)
})

router.post('/pincode/uploadfile', upload.single("uploadfile"),function (request,response,next){
    console.log('CSV file data has been uploaded in database');
    locationController.bulkUploadPincode(request,response,next)
})

module.exports = router;
