const express = require('express');
const router = express.Router();
const multer = require('multer')
const path = require('path')
const plantController = require('../src/controller/plant.controller');
const uservalidate = require('../middleware/validate.middelware'); 
const schemas = require('../src/validation/plant.validate'); 

//! Use of Multer
var storage = multer.diskStorage({
    destination: (req, file, callBack) => {
        callBack(null, './assets/uploads/')    
    },
    filename: (req, file, callBack) => {
        callBack(null, file.fieldname + '-' + Date.now() + path.extname(file.originalname))
    }
  })
  
  var upload = multer({
    storage: storage,
    fileFilter: (req, file, cb) => {
        if (file.mimetype == "text/csv") {
          cb(null, true);
        } else {
          cb(null, false);
          return cb(new Error('Only .csv format allowed!'));
        }
      }
  });

router.get('/', function (request, response, next) {
    plantController.getPlantList(request, response, next);
});

router.post("/create",uservalidate(schemas.plantCreate,''),function (request,response,next){
    plantController.createPlant(request,response,next)
})

router.post("/getById",uservalidate(schemas.plantGetById,''),function (request,response,next){
    plantController.getByIdPlant(request,response,next)
})

router.post("/update",uservalidate(schemas.plantUpdate,''),function (request,response,next){
    plantController.updatePlant(request,response,next)
})

router.post("/delete",uservalidate(schemas.plantDelete,''),function (request,response,next){
    plantController.deletePlant(request,response,next)
})

router.post('/uploadfile', upload.single("uploadfile"),function (request,response,next){
    console.log('CSV file data has been uploaded in mysql database ');
    plantController.bulkUploadPlant(request,response,next)
})


module.exports = router;
