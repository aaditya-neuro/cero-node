const express = require('express');
const router = express.Router();
const multer = require('multer')
const path = require('path')
const vehichleTypeController = require('../src/controller/vehicleType.controller');
const uservalidate = require('../middleware/validate.middelware'); 
const schemas = require('../src/validation/vehicleType.validate'); 

//! Use of Multer
var storage = multer.diskStorage({
    destination: (req, file, callBack) => {
        callBack(null, './assets/uploads/')    
    },
    filename: (req, file, callBack) => {
        callBack(null, file.fieldname + '-' + Date.now() + path.extname(file.originalname))
    }
  })
  
  var upload = multer({
    storage: storage,
    fileFilter: (req, file, cb) => {
        if (file.mimetype == "text/csv") {
          cb(null, true);
        } else {
          cb(null, false);
          return cb(new Error('Only .csv format allowed!'));
        }
      }
  });

router.get('/', function (request, response, next) {
    vehichleTypeController.getVehicleTypeList(request, response, next);
});

router.post("/create",uservalidate(schemas.vehicleTypeCreate,''),function (request,response,next){
    vehichleTypeController.createVehicleType(request,response,next)
})

router.post("/getById",uservalidate(schemas.vehicleTypeGetById,''),function (request,response,next){
    vehichleTypeController.getByIdVehicleType(request,response,next)
})

router.post("/update",uservalidate(schemas.vehicleTypeUpdate,''),function (request,response,next){
    vehichleTypeController.updateVehicleType(request,response,next)
})

router.post("/delete",uservalidate(schemas.vehicleTypeDelete,''),function (request,response,next){
    vehichleTypeController.deleteVehicleType(request,response,next)
})

router.post('/uploadfile', upload.single("uploadfile"),function (request,response,next){
    console.log('CSV file data has been uploaded in mysql database ');
    vehichleTypeController.bulkUploadVehicleType(request,response,next)
})


module.exports = router;
