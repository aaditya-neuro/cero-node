const express = require('express');
const router = express.Router();
const multer = require('multer')
const path = require('path')
const vehiclePlantStockController = require('../src/controller/vehiclePlantStock.controller');
const uservalidate = require('../middleware/validate.middelware'); 
const schemas = require('../src/validation/vehiclePlantStock.validate'); 

//! Use of Multer
var storage = multer.diskStorage({
    destination: (req, file, callBack) => {
        callBack(null, './assets/uploads/')    
    },
    filename: (req, file, callBack) => {
        callBack(null, file.fieldname + '-' + Date.now() + path.extname(file.originalname))
    }
  })
  
  var upload = multer({
    storage: storage,
    fileFilter: (req, file, cb) => {
        if (file.mimetype == "text/csv") {
          cb(null, true);
        } else {
          cb(null, false);
          return cb(new Error('Only .csv format allowed!'));
        }
      }
  });

router.get('/', function (request, response, next) {
    vehiclePlantStockController.getVehiclePlantStockList(request, response, next);
});

router.post("/create",uservalidate(schemas.vehiclePlantStockCreate,''),function (request,response,next){
    vehiclePlantStockController.createVehiclePlantStock(request,response,next)
})

router.post("/getById",uservalidate(schemas.vehiclePlantStockGetById,''),function (request,response,next){
    vehiclePlantStockController.getByIdVehiclePlantStock(request,response,next)
})

router.post("/update",uservalidate(schemas.vehiclePlantStockUpdate,''),function (request,response,next){
    vehiclePlantStockController.updateVehiclePlantStock(request,response,next)
})

router.post("/delete",uservalidate(schemas.vehiclePlantStockDelete,''),function (request,response,next){
    vehiclePlantStockController.deleteVehiclePlantStock(request,response,next)
})

router.post('/uploadfile', upload.single("uploadfile"),function (request,response,next){
    console.log('CSV file data has been uploaded in mysql database ');
    vehiclePlantStockController.bulkUploadVehiclePlantStock(request,response,next)
})


module.exports = router;
