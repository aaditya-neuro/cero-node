const express = require('express');
const router = express.Router();
const multer = require('multer')
const path = require('path')
const makeModelController = require('../src/controller/makemodel.controller');
const uservalidate = require('../middleware/validate.middelware'); 
const schemas = require('../src/validation/makemodel.validate'); 

//! Use of Multer
var storage = multer.diskStorage({
    destination: (req, file, callBack) => {
        callBack(null, './assets/uploads/')    
    },
    filename: (req, file, callBack) => {
        callBack(null, file.fieldname + '-' + Date.now() + path.extname(file.originalname))
    }
  })
  
  var upload = multer({
    storage: storage,
    fileFilter: (req, file, cb) => {
        if (file.mimetype == "text/csv") {
          cb(null, true);
        } else {
          cb(null, false);
          return cb(new Error('Only .csv format allowed!'));
        }
      }
  });

router.get('/make', function (request, response, next) {
    makeModelController.getMakeList(request, response, next);
});

router.post("/make/create",uservalidate(schemas.makeCreate,''),function (request,response,next){
    makeModelController.createMake(request,response,next)
})

router.post("/make/getById",uservalidate(schemas.makeGetById,''),function (request,response,next){
    makeModelController.getByIdMake(request,response,next)
})

router.post("/make/update",uservalidate(schemas.makeUpdate,''),function (request,response,next){
    makeModelController.updateMake(request,response,next)
})

router.post("/make/delete",uservalidate(schemas.makeDelete,''),function (request,response,next){
    makeModelController.deleteMake(request,response,next)
})

router.get('/model', function (request, response, next) {
    makeModelController.getModelList(request, response, next);
});

router.post("/model/create",uservalidate(schemas.modelCreate,''),function (request,response,next){
    makeModelController.createModel(request,response,next)
})

router.post("/model/getById",uservalidate(schemas.modelGetById,''),function (request,response,next){
    makeModelController.getByIdModel(request,response,next)
})

router.post("/model/update",uservalidate(schemas.modelUpdate,''),function (request,response,next){
    makeModelController.updateModel(request,response,next)
})

router.post("/model/delete",uservalidate(schemas.modelDelete,''),function (request,response,next){
    makeModelController.deleteModel(request,response,next)
})


module.exports = router;