const express = require('express');
const router = express.Router();
const multer = require('multer')
const path = require('path')
const vehichleScrapPriceController = require('../src/controller/vehicleScrapPrice.controller');
const uservalidate = require('../middleware/validate.middelware'); 
// const schemas = require('../src/validation/vehicleScrapPrice.validate'); 

//! Use of Multer
var storage = multer.diskStorage({
    destination: (req, file, callBack) => {
        callBack(null, './assets/uploads/')    
    },
    filename: (req, file, callBack) => {
        callBack(null, file.fieldname + '-' + Date.now() + path.extname(file.originalname))
    }
  })
  
  var upload = multer({
    storage: storage,
    fileFilter: (req, file, cb) => {
        if (file.mimeScrapPrice == "text/csv") {
          cb(null, true);
        } else {
          cb(null, false);
          return cb(new Error('Only .csv format allowed!'));
        }
      }
  });

router.get('/', function (request, response, next) {
    vehichleScrapPriceController.getVehicleScrapPriceList(request, response, next);
});

router.post("/create",function (request,response,next){
    vehichleScrapPriceController.createVehicleScrapPrice(request,response,next)
})

router.post("/getById",function (request,response,next){
    vehichleScrapPriceController.getByIdVehicleScrapPrice(request,response,next)
})

router.post("/update",function (request,response,next){
    vehichleScrapPriceController.updateVehicleScrapPrice(request,response,next)
})

router.post("/delete",function (request,response,next){
    vehichleScrapPriceController.deleteVehicleScrapPrice(request,response,next)
})

router.post('/uploadfile', upload.single("uploadfile"),function (request,response,next){
    console.log('CSV file data has been uploaded in mysql database ');
    vehichleScrapPriceController.bulkUploadVehicleScrapPrice(request,response,next)
})


module.exports = router;
