const express = require('express');
const router = express.Router();
const moduleController = require('../src/controller/moduleAction.controller');
const uservalidate = require('../middleware/validate.middelware'); 
const schemas = require('../src/validation/moduleAction.validate'); 
router.get('/', function (request, response, next) {
    moduleController.getModuleActions(request, response, next);
});
router.post('/create',uservalidate(schemas.moduleActionCreate,''), function (request, response, next) {
    moduleController.createModuleAction(request, response, next);
});
// router.post('/moduleMappedActions',uservalidate(schemas.moduleMappedActions,''), function (request, response, next) {
//     moduleController.moduleMappedActions(request, response, next);
// });



module.exports = router;
