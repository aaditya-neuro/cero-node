const express = require('express');
const router = express.Router();
const multer = require('multer')
const path = require('path')
const baseRateController = require('../src/controller/baseRate.controller');
const uservalidate = require('../middleware/validate.middelware'); 
const schemas = require('../src/validation/baseRate.validate'); 

//! Use of Multer
var storage = multer.diskStorage({
    destination: (req, file, callBack) => {
        callBack(null, './assets/uploads/')    
    },
    filename: (req, file, callBack) => {
        callBack(null, file.fieldname + '-' + Date.now() + path.extname(file.originalname))
    }
  })
  
  var upload = multer({
    storage: storage,
    fileFilter: (req, file, cb) => {
        if (file.mimetype == "text/csv") {
          cb(null, true);
        } else {
          cb(null, false);
          return cb(new Error('Only .csv format allowed!'));
        }
      }
  });

router.get('/', function (request, response, next) {
    baseRateController.getBaseRateList(request, response, next);
});

router.post("/create",uservalidate(schemas.baseRateCreate,''),function (request,response,next){
    baseRateController.createBaseRate(request,response,next)
})

router.post("/getById",uservalidate(schemas.baseRateGetById,''),function (request,response,next){
    baseRateController.getByIdBaseRate(request,response,next)
})

router.post("/update",uservalidate(schemas.baseRateUpdate,''),function (request,response,next){
    baseRateController.updateBaseRate(request,response,next)
})

router.post("/delete",uservalidate(schemas.baseRateDelete,''),function (request,response,next){
    baseRateController.deleteBaseRate(request,response,next)
})

router.post('/uploadfile', upload.single("uploadfile"),function (request,response,next){
    console.log('CSV file data has been uploaded in mysql database ');
    baseRateController.bulkUploadBaseRate(request,response,next)
})


module.exports = router;
