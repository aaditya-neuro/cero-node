'use strict';
const {
  Model
} = require('sequelize');
module.exports = (sequelize, DataTypes) => {
  class cero_model_master extends Model {
    /**
     * Helper method for defining associations.
     * This method is not a part of Sequelize lifecycle.
     * The `models/index` file will call this method automatically.
     */
    static associate(models) {
      // define association here
    }
  }
  cero_model_master.init({
    rm_code: DataTypes.STRING,
    make_id: DataTypes.INTEGER,
    model: DataTypes.STRING,
    material_desc: DataTypes.STRING,
    vehicle_category_id: DataTypes.INTEGER,
    uom: DataTypes.STRING,
    old_material_number: DataTypes.STRING,
    max_weight: DataTypes.INTEGER,
    is_model_approved: DataTypes.BOOLEAN,
    model_approved_by: DataTypes.INTEGER,
    approved_date: DataTypes.DATE,
    data_pushed_in_sap: DataTypes.BOOLEAN,
    sap_response: DataTypes.STRING,
    data_pushed_by: DataTypes.INTEGER,
    data_pushed_date: DataTypes.DATE,
    created_by: DataTypes.INTEGER,
    updated_by: DataTypes.INTEGER
  }, {
    sequelize,
    modelName: 'cero_model_master',
  });
  return cero_model_master;
};