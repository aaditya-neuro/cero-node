'use strict';
module.exports = (sequelize, DataTypes,Sequelize) => {
  const VehiclePlantStockMaster = sequelize.define('VehiclePlantStockMaster', {
    id: {
      allowNull: false,
      autoIncrement: true,
      primaryKey: true,
      type: DataTypes.INTEGER
    },
    plantId: {
      type: DataTypes.INTEGER
    },
    rmCode: {
      type: DataTypes.STRING,
    },
    stockLevel: {
         type: DataTypes.STRING
    },
    demand: {
         type: DataTypes.STRING
    },
    created_by: {
         type: DataTypes.INTEGER
    },
    updated_by: {
         type: DataTypes.INTEGER
    },
    status: {
         type: DataTypes.INTEGER
    },  
    createdAt: {
      allowNull: false,
      type: DataTypes.DATE
    },
    updatedAt: {
      allowNull: false,
      type: DataTypes.DATE
    }
  }, {});
  return VehiclePlantStockMaster;
};
