'use strict';
const {
  Model
} = require('sequelize');
module.exports = (sequelize, DataTypes) => {
  class LeadVehicleDetails extends Model {
    /**
     * Helper method for defining associations.
     * This method is not a part of Sequelize lifecycle.
     * The `models/index` file will call this method automatically.
     */
    static associate(models) {
      // define association here
    }
  }
  LeadVehicleDetails.init({
    lead_id: DataTypes.INTEGER,
    vehicle_category: DataTypes.STRING,
    vehicle_type: DataTypes.STRING,
    make: DataTypes.STRING,
    registration_year: DataTypes.INTEGER,
    model: DataTypes.STRING,
    material_code: DataTypes.STRING,
    material_group: DataTypes.STRING,
    kerb_weight: DataTypes.STRING,
    scrap_price: DataTypes.NUMERIC,
    variant: DataTypes.STRING,
    fuel_type: DataTypes.STRING,
    cng_lpg_addition: DataTypes.STRING,
    vehicle_condition: DataTypes.STRING,
    month_year_of_manufacture: DataTypes.DATE,
    registration_number: DataTypes.TEXT,
    date_of_registration: DataTypes.DATE,
    engine_number: DataTypes.STRING,
    chassis_number: DataTypes.STRING,
    rc_expiry_date: DataTypes.DATE,
    is_Seller_and_rc_owner_different: DataTypes.STRING,
    base_price: DataTypes.NUMERIC,
    threshold_value: DataTypes.NUMERIC,
    upper_max_price: DataTypes.NUMERIC,
    demanded_price: DataTypes.NUMERIC,
    final_price: DataTypes.NUMERIC,
    comment: DataTypes.TEXT,
    approval_required: DataTypes.BOOLEAN,
    approval_required_from: DataTypes.INTEGER,
    approval_from_user_id: DataTypes.INTEGER,
    approval_request_date: DataTypes.DATE,
    approval_status: DataTypes.STRING,
    approval_date: DataTypes.DATE,
    approval_comment: DataTypes.STRING,
    deal_status: DataTypes.STRING,
    deal_convertted_date: DataTypes.DATE,
    converted_by: DataTypes.INTEGER,
    updated_date: DataTypes.DATE,
    updated_by: DataTypes.INTEGER,
    created_by: DataTypes.INTEGER,
    closed_by: DataTypes.INTEGER,
    effective_from: DataTypes.DATE,
    effective_to: DataTypes.DATE,
    is_current: DataTypes.STRING
  }, {
    sequelize,
    modelName: 'LeadVehicleDetails',
  });
  return LeadVehicleDetails;
};