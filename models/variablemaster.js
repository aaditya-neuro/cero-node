'use strict';
module.exports = (sequelize, DataTypes,Sequelize) => {
  const VariableMaster = sequelize.define('VariableMaster', {
    id: {
      allowNull: false,
      autoIncrement: true,
      primaryKey: true,
      type: DataTypes.INTEGER
    },
    variableType: {
      type: DataTypes.STRING
    },
    variableValue: {
      type: DataTypes.INTEGER,
    },
    price: {
         type: DataTypes.INTEGER
    },
    created_by: {
         type: DataTypes.INTEGER
    },
    updated_by: {
         type: DataTypes.INTEGER
    },
    status: {
         type: DataTypes.INTEGER
    },  
    createdAt: {
      allowNull: false,
      type: DataTypes.DATE
    },
    updatedAt: {
      allowNull: false,
      type: DataTypes.DATE
    }
  }, {});
  return VariableMaster;
};
