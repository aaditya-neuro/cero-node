'use strict';
const {
  Model
} = require('sequelize');
module.exports = (sequelize, DataTypes) => {
  class cero_make_master extends Model {
    /**
     * Helper method for defining associations.
     * This method is not a part of Sequelize lifecycle.
     * The `models/index` file will call this method automatically.
     */
    static associate(models) {
      // define association here
    }
  }
  cero_make_master.init({
    make: DataTypes.STRING,
    make_sap_code: DataTypes.STRING,
    status: DataTypes.INTEGER,
    vehicle_category_id: DataTypes.ARRAY(DataTypes.INTEGER),
    created_by: DataTypes.INTEGER,
    updated_by: DataTypes.INTEGER
  }, {
    sequelize,
    modelName: 'cero_make_master',
  });
  return cero_make_master;
};