'use strict';
module.exports = (sequelize, DataTypes,Sequelize) => {
  const Modules = sequelize.define('Modules', {
    id: {
      allowNull: false,
      autoIncrement: true,
      primaryKey: true,
      type: DataTypes.INTEGER
    },
    moduleName: {
      allowNull: false,
      type: DataTypes.STRING,
      unique: true
    },
    moduleLevel: {
      allowNull: false,
      type: DataTypes.STRING,
      unique: true
    },
    sequence: {
      allowNull: false,
      type: DataTypes.INTEGER,
    },
    icon: {
      type: DataTypes.STRING,
    },
    status: {
         type: DataTypes.INTEGER,
         defaultValue: 1
    },
    isDeleted: {
      type: DataTypes.INTEGER,
      allowNull: false,
     
    },
  
    createdAt: {
      allowNull: false,
      type: DataTypes.DATE
    },
    updatedAt: {
      allowNull: false,
      type: DataTypes.DATE
    }
  }, {});
  Modules.associate = function(models) {
   // Doctor.belongsTo(models.User, {foreignKey: 'userId'});
   // Doctor.hasMany(models.DoctorExperience, {foreignKey: 'doctorId'});
   Modules.hasMany(models.ModulesActions, {foreignKey: 'moduleId'});
   models.ModulesActions.belongsTo(Modules, {foreignKey: 'moduleId'});
  };
  return Modules;
};
