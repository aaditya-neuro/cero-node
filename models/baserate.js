'use strict';
module.exports = (sequelize, DataTypes,Sequelize) => {
  const BaseRate = sequelize.define('BaseRate', {
    id: {
      allowNull: false,
      autoIncrement: true,
      primaryKey: true,
      type: DataTypes.INTEGER
    },
    vehicleTypeId: {
      type: DataTypes.INTEGER
    },
    vehicleCategoryId: {
      type: DataTypes.INTEGER,
    },
    basePrice: {
         type: DataTypes.INTEGER
    },
    relativePrice: {
         type: DataTypes.INTEGER
    },
    created_by: {
         type: DataTypes.INTEGER
    },
    updated_by: {
         type: DataTypes.INTEGER
    },  
    status: {
      type: DataTypes.INTEGER
    },
    createdAt: {
      allowNull: false,
      type: DataTypes.DATE
    },
    updatedAt: {
      allowNull: false,
      type: DataTypes.DATE
    }
  }, {});
  return BaseRate;
};
