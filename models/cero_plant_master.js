'use strict';
const {
  Model
} = require('sequelize');
module.exports = (sequelize, DataTypes) => {
  class cero_plant_master extends Model {
    /**
     * Helper method for defining associations.
     * This method is not a part of Sequelize lifecycle.
     * The `models/index` file will call this method automatically.
     */
    static associate(models) {
      // define association here
    }
  }
  cero_plant_master.init({
    plant_name: DataTypes.STRING,
    state: DataTypes.INTEGER,
    city: DataTypes.INTEGER,
    address: DataTypes.STRING,
    serviceable_city: DataTypes.ARRAY(DataTypes.INTEGER),
    Created_by: DataTypes.INTEGER,
    updated_by: DataTypes.INTEGER,
    status: DataTypes.INTEGER
  }, {
    sequelize,
    modelName: 'cero_plant_master',
  });
  return cero_plant_master;
};