'use strict';
module.exports = (sequelize, DataTypes,Sequelize) => {
  const ModulesActions = sequelize.define('ModulesActions', {
    id: {
      allowNull: false,
      autoIncrement: true,
      primaryKey: true,
      type: DataTypes.INTEGER
    },
    moduleId:{
       allowNull:false,
       type: DataTypes.INTEGER
    },
    actionName: {
      allowNull: false,
      type: DataTypes.STRING,
    },
    actionLevel: {
      allowNull: false,
      type: DataTypes.STRING,
    },
    sequence: {
      allowNull: false,
      type: DataTypes.INTEGER,
    },
    icon: {
      type: DataTypes.STRING,
    },
    status: {
         type: DataTypes.INTEGER,
         defaultValue: 1
    },
    isDeleted: {
      type: DataTypes.INTEGER,
      defaultValue: 0
    },
  
    createdAt: {
      allowNull: false,
      type: DataTypes.DATE
    },
    updatedAt: {
      allowNull: false,
      type: DataTypes.DATE
    }
  }, {});
   ModulesActions.associate = function(models) {
   ModulesActions.belongsTo(models.Modules, {foreignKey: 'moduleId'});
   // Doctor.hasMany(models.DoctorExperience, {foreignKey: 'doctorId'});
   //Modules.hasMany(models.ModulesActions, {foreignKey: 'moduleId'});
  };
  return ModulesActions;
};
