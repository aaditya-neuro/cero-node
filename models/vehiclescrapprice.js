'use strict';
const {
  Model
} = require('sequelize');
module.exports = (sequelize, DataTypes) => {
  class VehicleScrapPrice extends Model {
    /**
     * Helper method for defining associations.
     * This method is not a part of Sequelize lifecycle.
     * The `models/index` file will call this method automatically.
     */
    static associate(models) {
      // define association here
    }
  }
  VehicleScrapPrice.init({
    vehicleCategoryId: DataTypes.INTEGER,
    vehicleTypeId: DataTypes.INTEGER,
    vehicleScrapBasePrice: DataTypes.INTEGER,
    status: DataTypes.INTEGER
  }, {
    sequelize,
    modelName: 'VehicleScrapPrice',
  });
  return VehicleScrapPrice;
};