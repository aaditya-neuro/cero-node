'use strict';
module.exports = (sequelize, DataTypes,Sequelize) => {
  const Roles = sequelize.define('Roles', {
    id: {
      allowNull: false,
      autoIncrement: true,
      primaryKey: true,
      type: DataTypes.INTEGER
    },

    permission: {
      allowNull: false,
      type: DataTypes.TEXT,
      unique: true
    },
    roleName: {
      allowNull: false,
      type: DataTypes.STRING,
      unique: true
    },
    roleSlug: {
      allowNull: false,
      type: DataTypes.STRING,
      unique: true
    },

    sequence: {
      allowNull: false,
      type: DataTypes.INTEGER,
    },
    parentId: {
      type: DataTypes.INTEGER,
    },
    status: {
         type: DataTypes.INTEGER,
         defaultValue: 1
    },
    isDeleted: {
      type: DataTypes.INTEGER,
      allowNull: false,
     
    },
  
    createdAt: {
      allowNull: false,
      type: DataTypes.DATE
    },
    updatedAt: {
      allowNull: false,
      type: DataTypes.DATE
    }
  }, {});
  Roles.associate = function(models) {
  
    Roles.hasMany(models.Users, {foreignKey: 'roleId'});
  };
  return Roles;
};
