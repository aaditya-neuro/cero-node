'use strict';
module.exports = (sequelize, DataTypes,Sequelize) => {
  const AccesoriesMaster = sequelize.define('AccesoriesMaster', {
    id: {
      allowNull: false,
      autoIncrement: true,
      primaryKey: true,
      type: DataTypes.INTEGER
    },
    accessoriesName: {
      type: DataTypes.STRING
    },
    accessoriesPrice: {
      type: DataTypes.INTEGER,
    },
    status: {
         type: DataTypes.INTEGER
    },
    created_by: {
         type: DataTypes.INTEGER
    },
    updated_by: {
         type: DataTypes.INTEGER
    },
    createdAt: {
      allowNull: false,
      type: DataTypes.DATE
    },
    updatedAt: {
      allowNull: false,
      type: DataTypes.DATE
    }
  }, {});
  return AccesoriesMaster;
};
