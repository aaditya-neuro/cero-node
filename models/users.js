'use strict';
module.exports = (sequelize, DataTypes,Sequelize) => {
  const Users = sequelize.define('Users', {
    id: {
      allowNull: false,
      autoIncrement: true,
      primaryKey: true,
      type: DataTypes.INTEGER
    },

    userName: {
      allowNull: false,
      type: DataTypes.STRING,
      unique: true
    },
    userEmail: {
      allowNull: false,
      type: DataTypes.STRING,
      unique: true
    },
    userMobile: {
      allowNull: false,
      type: DataTypes.STRING,
      unique: true
    },
    userPassword: {
      allowNull: false,
      type: DataTypes.STRING,
      unique: true
    },
    userAddress: {
      allowNull: false,
      type: DataTypes.STRING,
      unique: true
    },
    roleId: {
      allowNull: false,
      type: DataTypes.INTEGER,
      unique: true
    },
    status: {
         type: DataTypes.INTEGER,
         defaultValue: 1
    },
    isDeleted: {
      type: DataTypes.INTEGER,
      allowNull: false,
     
    },
  
    createdAt: {
      allowNull: false,
      type: DataTypes.DATE
    },
    updatedAt: {
      allowNull: false,
      type: DataTypes.DATE
    }
  }, {});
  Users.associate = function(models) {
    Users.belongsTo(models.Roles, {foreignKey: 'roleId'});
  };
  return Users;
};
